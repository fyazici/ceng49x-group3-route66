# Simple script to prepare a deployment archive
import subprocess
import pathlib
import sys
import zipfile
import time
import shutil
import argparse

def zip_write_tree(z, d, pred=lambda x: True):
    for p in pathlib.Path(d).rglob("*"):
        if not p.is_dir() and pred(str(p)):
            z.write(str(p))

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--no-build", action="store_true")
    parser.add_argument("--no-redist", action="store_true")
    args = parser.parse_args()

    z = zipfile.ZipFile("route66-deployment.zip".format(int(time.time())), "w")

    # add scripts
    z.write("install.py")
    z.write("install.bat")
    z.write("run.py")
    z.write("run.bat")

    # add backend ($ ls -1 api/)
    z.write("api/requirements.txt")
    z.writestr(zipfile.ZipInfo("api/static/log_file/"), "")
    z.write("api/base.py")
    z.write("api/helpers.py")
    z.write("api/main.py")
    z.write("api/models_.py")
    z.write("api/render_server_views.py")
    z.write("api/serializers.py")
    z.write("api/views.py")

    # add frontend
    z.write("ui/requirements.txt")
    z.write("ui/react-server.py")
    if not args.no_build:
        subprocess.call(["npm", "i"], cwd="frontend/")
        subprocess.call(["npm", "run", "build"], cwd="frontend/")
    shutil.rmtree("ui/templates/")
    shutil.copytree("frontend/build", "ui/templates")
    zip_write_tree(z, "ui/templates/")

    # add render server
    z.write("renderer/requirements.txt")
    z.write("renderer/render-server.py")
    z.writestr(zipfile.ZipInfo("renderer/static/task/"), "")

    z.write("renderer/cesium/main.py")
    z.write("renderer/cesium/static/cesium-renderer.html")
    zip_write_tree(z, "renderer/cesium/static/js/")
    zip_write_tree(z, "renderer/cesium/test/")
    z.write("renderer/cesium/headless_profiles/create_profiles.py")
    zip_write_tree(z, "renderer/cesium/headless_profiles/headless_default/")

    z.write("renderer/depth/main.py")
    z.writestr(zipfile.ZipInfo("renderer/depth/dem/"), "")
    z.write("renderer/depth/util/__init__.py")
    z.write("renderer/depth/util/depth_mapper.py")
    z.write("renderer/depth/util/earth_raytracer.py")
    z.write("renderer/depth/util/fast_dem_provider.pyx")
    z.write("renderer/depth/util/setup.py")
    z.write("renderer/depth/util/wgs84.json")
    z.write("renderer/depth/util/depth-gen-gpu-kernel.cu")
    z.write("renderer/depth/util/depth-map-gpu-kernel.cu")
    zip_write_tree(z, "renderer/depth/test/")

    z.write("renderer/earthpro/main.py")
    z.write("renderer/earthpro/util/__init__.py")
    z.write("renderer/earthpro/util/localization.py")
    z.write("renderer/earthpro/util/mlog2kml.py")
    zip_write_tree(z, "renderer/earthpro/test/")

    z.write("renderer/effects/fog_process.py")
    z.write("renderer/effects/modules/__init__.py")
    z.write("renderer/effects/modules/f_perlin.c")
    z.write("renderer/effects/modules/f_perlin.dll")
    z.write("renderer/effects/modules/FogFactor.py")
    z.write("renderer/effects/modules/Perlin.py")
    z.write("renderer/effects/modules/Sample.py")
    z.write("renderer/effects/modules/setup.py")
    z.write("renderer/effects/modules/vec3.cpp")
    z.write("renderer/effects/modules/worley_without_glm.cpp")
    z.write("renderer/effects/modules/worley_without_glm.dll")
    z.write("renderer/effects/test.py")

    if pathlib.Path("redist/").exists():
        if not args.no_redist:
            zip_write_tree(z, "redist/")
        else:
            p = pathlib.Path()
            redist_files = list(str(p.relative_to("redist")) for p in pathlib.Path("redist").rglob("*") if not p.is_dir())
            z.writestr("redist/DEPS.txt", "\n".join(redist_files))

    z.write("README.txt")
    z.close()