import argparse
import json
import util
import subprocess
import os
import pathlib

try:
    DEM_PATH = os.environ["R66_DEM_PATH"]
except:
    DEM_PATH = "./dem/"

try:
    CACHE_LIMIT = int(os.environ["R66_CACHE_LIMIT"])
except:
    CACHE_LIMIT = 100

try:
    FFMPEG_EXECUTABLE = os.environ["R66_FFMPEG_EXECUTABLE"]
except KeyError:
    FFMPEG_EXECUTABLE = r"ffmpeg"

def render(task_info, cam_conf, output_dir):
    # TODO: add scaling to task info in db
    raytracer = util.EllipsoidRaytracer(task_info, scaling=1)
    elevation = util.FastDemProvider(DEM_PATH, capacity_tiles=CACHE_LIMIT)
    dmapper = util.DepthMapper(task_info, scaling=1)

    # A crf value of 17 is visually lossless (though not technically lossless)
    # source: ffmpeg docs
    # Not using h264_nvenc here: out of memory (2xCUDA @1080p + 3xH264 @1080p)
    valid_proc = subprocess.Popen([
        FFMPEG_EXECUTABLE, "-v", "warning", "-stats", "-y", "-f", "rawvideo", "-pixel_format", "gray",
        "-framerate", str(task_info["framerate"]),
        "-s:v:1", "{}x{}".format(raytracer._width, raytracer._height),
        "-i", "-", "-c:v", "h264", "-pix_fmt", "yuv420p", "-crf", "17",
        str(pathlib.Path(output_dir).joinpath("valid.mp4"))
    ], stdin=subprocess.PIPE, stderr=subprocess.DEVNULL)

    depth_proc = subprocess.Popen([
        FFMPEG_EXECUTABLE, "-v", "warning", "-stats", "-y", "-f", "rawvideo", "-pixel_format", "gray",
        "-framerate", str(task_info["framerate"]),
        "-s:v:1", "{}x{}".format(raytracer._width, raytracer._height),
        "-i", "-", "-c:v", "h264", "-pix_fmt", "yuv420p", "-crf", "17",
        str(pathlib.Path(output_dir).joinpath("depth.mp4"))
    ], stdin=subprocess.PIPE, stderr=subprocess.DEVNULL)

    altitude_proc = subprocess.Popen([
        FFMPEG_EXECUTABLE, "-v", "warning", "-stats", "-y", "-f", "rawvideo", "-pixel_format", "gray",
        "-framerate", str(task_info["framerate"]),
        "-s:v:1", "{}x{}".format(raytracer._width, raytracer._height),
        "-i", "-", "-c:v", "h264", "-pix_fmt", "yuv420p", "-crf", "17",
        str(pathlib.Path(output_dir).joinpath("altitude.mp4"))
    ], stdin=subprocess.PIPE)

    camconf_jsons = open(cam_conf, "r")
    # declared for reuse of allocated buffer
    rt_result = raytracer.generate_buffers()
    map_result = dmapper.generate_buffers()

    for line in camconf_jsons:
        camconf = json.loads(line)
        raytracer.execute(camconf, rt_result)
        elevation.fill_elevation_frame(rt_result)
        dmapper.execute(camconf, rt_result, map_result)

        # if this becomes a speed bottleneck, use a separate thread for second write
        valid_proc.stdin.write(rt_result[0].data)
        depth_proc.stdin.write(map_result[0].data)
        altitude_proc.stdin.write(map_result[1].data)



if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--task-info", "-t")
    parser.add_argument("--cam-conf", "-c")
    parser.add_argument("--output-dir", "-o")
    args = parser.parse_args()

    render(json.loads(args.task_info), args.cam_conf, args.output_dir)
