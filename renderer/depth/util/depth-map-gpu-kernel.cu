#include <stdint.h>

__device__ double Dot3(double3 a, double3 b) { return a.x*b.x+a.y*b.y+a.z*b.z; }
__device__ double Mag3(double3 a) { return sqrt(Dot3(a, a)); }
__device__ double3 Scal3(double3 a, double b) { return make_double3(a.x*b, a.y*b, a.z*b); }
__device__ double3 Mul3(double3 a, double3 b) { return make_double3(a.x*b.x, a.y*b.y, a.z*b.z); }
__device__ double3 Add3(double3 a, double3 b) { return make_double3(a.x+b.x, a.y+b.y, a.z+b.z); }
__device__ double3 AddS3(double3 a, double b) { return make_double3(a.x+b, a.y+b, a.z+b); }
__device__ double3 Sub3(double3 a, double3 b) { return make_double3(a.x-b.x, a.y-b.y, a.z-b.z); }
__device__ double3 Normalize3(double3 a) { return Scal3(a, 1.0/Mag3(a)); }
__device__ double3 Inv3(double3 a) { return make_double3(1.0/a.x, 1.0/a.y, 1.0/a.z); }
__device__ double Rad2Deg(double a) { return a * 57.2957795130823208768; }
__device__ double Clamp(double x, double a, double b) { return (x < a) ? a : ((x > b) ? b : x); }

__global__ void depth_map_ellipsoid_impl(
    int width, int height, 
    double3 posWC,
    double3 inverseRadiiSquared,
    double logmap_zero,
    uint8_t *valid_in,
    double *cartesian_in,
    uint16_t *altitude_in,
    uint8_t *depth_out,
    uint8_t *altitude_out
    )
{
    int yy = blockIdx.x * blockDim.x + threadIdx.x;
    int xx = blockIdx.y * blockDim.y + threadIdx.y;

    if (yy >= height || xx >= width) return;
    int i = yy * width + xx;
    int i3 = 3 * i;

    if (valid_in[i] != 0)  // valid mask
    {
        
        // calculate ellipsoid surface normal
        double3 p = make_double3(cartesian_in[i3 + 0], cartesian_in[i3 + 1], cartesian_in[i3 + 2]);
        double3 n = Normalize3(Mul3(p, inverseRadiiSquared));
        p = Add3(p, Scal3(n, altitude_in[i]));
        double d_dist = Mag3(Sub3(p, posWC));
        double d_alt = (double) altitude_in[i];
        // 60000 instead of 65535 to prevent rounding overflow in 8-bits (255-256)
        d_dist = Clamp(d_dist / logmap_zero, 0.0, 60000.0);
        d_alt = Clamp(d_alt / logmap_zero, 0.0, 60000.0);

        // TODO: optimize this mapping for meaningful ranges
        uint16_t dist_8 = 16 * log2(d_dist);
        uint16_t alt_8 = 16 * log2(d_alt);
        
        depth_out[i] = dist_8;
        altitude_out[i] = alt_8;
    }
    else
    {
        depth_out[i] = 0;
        altitude_out[i] = 0;
    }
}