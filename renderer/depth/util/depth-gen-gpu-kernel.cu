#include <stdint.h>

__device__ double Dot3(double3 a, double3 b) { return a.x*b.x+a.y*b.y+a.z*b.z; }
__device__ double Mag3(double3 a) { return sqrt(Dot3(a, a)); }
__device__ double3 Scal3(double3 a, double b) { return make_double3(a.x*b, a.y*b, a.z*b); }
__device__ double3 Mul3(double3 a, double3 b) { return make_double3(a.x*b.x, a.y*b.y, a.z*b.z); }
__device__ double3 Add3(double3 a, double3 b) { return make_double3(a.x+b.x, a.y+b.y, a.z+b.z); }
__device__ double3 AddS3(double3 a, double b) { return make_double3(a.x+b, a.y+b, a.z+b); }
__device__ double3 Sub3(double3 a, double3 b) { return make_double3(a.x-b.x, a.y-b.y, a.z-b.z); }
__device__ double3 Normalize3(double3 a) { return Scal3(a, 1.0/Mag3(a)); }
__device__ double3 Inv3(double3 a) { return make_double3(1.0/a.x, 1.0/a.y, 1.0/a.z); }
__device__ double Rad2Deg(double a) { return a * 57.2957795130823208768; }

__device__ double3 ScaleToGeodeticSurface(
    double3 cartesian, 
    double3 inverseRadii, double3 inverseRadiiSquared, 
    double centerToleranceSquared
    )
{
    double3 pos2 = Mul3(cartesian, Mul3(cartesian, Mul3(inverseRadii, inverseRadii)));
    double squaredNorm = pos2.x + pos2.y + pos2.z;
    double ratio = sqrt(1.0 / squaredNorm);
    double3 intersection = Scal3(cartesian, ratio);

    // skip near center check

    double3 gradient = Scal3(Mul3(intersection, inverseRadiiSquared), 2.0);
    double lambda = (1.0 - ratio) * Mag3(cartesian) / (0.5 * Mag3(gradient));
    double correction = 0.0;

    double func;
    double denominator;
    double3 multiplier, multiplier2, multiplier3;

    // loop until required numerical accuracy is achieved by iteration
    do {
        lambda -= correction;
        
        multiplier = Inv3(AddS3(Scal3(inverseRadiiSquared, lambda), 1.0));
        multiplier2 = Mul3(multiplier, multiplier);
        multiplier3 = Mul3(multiplier2, multiplier);

        func = Dot3(pos2, Mul3(multiplier3, inverseRadiiSquared));
        double derivative = -2.0 * denominator;
        correction = func / derivative;
    } while (abs(func) > 1.0e-12);

    return Mul3(cartesian, multiplier);
}

__global__ void pick_ellipsoid_3d_impl(
    int width, int height, 
    double tan_theta, double tan_phi, double near,
    double3 posWC, double3 dirWC, 
    double3 rightWC, double3 upWC,
    double3 inverseRadii, double3 inverseRadiiSquared,
    double centerToleranceSquared,
    uint8_t *valid_out,
    double *cartesian_out,
    double *cartographic_out
    )
{
    int yy = blockIdx.x * blockDim.x + threadIdx.x;
    int xx = blockIdx.y * blockDim.y + threadIdx.y;

    if (yy >= height || xx >= width) return;
    int i = yy * width + xx;
    int i2 = 2 * i;
    int i3 = 3 * i;

    // get pick ray impl
    // double tan_phi = tan(fovy * 0.5);
    // double tan_theta = aspectRatio * tan_phi;
    double x = (2.0 / width) * xx - 1.0;
    double y = (2.0 / height) * (height - yy) - 1.0;
    double3 near_center = Add3(Scal3(dirWC, near), posWC);
    double3 x_dir = Scal3(rightWC, x * near * tan_theta);
    double3 y_dir = Scal3(upWC, y * near * tan_phi);
    double3 position = posWC;
    double3 direction = Normalize3(Add3(near_center, Add3(x_dir, Sub3(y_dir, posWC))));

    // intersect wgs84 impl
    double3 q = Mul3(inverseRadii, position);
    double3 w = Mul3(inverseRadii, direction);
    double q2 = Dot3(q, q);
    double qw = Dot3(q, w);
    double w2 = Dot3(w, w);
    double qw2 = qw * qw;
    double difference = q2 - 1.0;
    double product = w2 * difference;
    double discriminant = qw * qw - product;

    // test
    if (q2 > 1.0 && qw < 0.0 && qw2 > product)
    {
        double temp = -qw + sqrt(discriminant);
        double root0 = temp / w2;
        double root1 = difference / temp;

        double start = min(root0, root1);
        double stop = max(root0, root1);
        double t = (start > 0) ? start : stop;

        // convert ECEF to WGS84 lat-lon-alt
        double3 cartesian = Add3(position, Scal3(direction, t));
        double3 p = ScaleToGeodeticSurface(cartesian, inverseRadii, inverseRadiiSquared, centerToleranceSquared);
        double3 n = Normalize3(Mul3(p, inverseRadiiSquared));
        // not used since it will be queried in detail
        //double3 h = Sub3(cartesian, p);

        double longitude = atan2(n.y, n.x);
        double latitude = asin(n.z);
        // not used since it will be queried in detail
        // double height = sign(Dot3(h, cartesian)) * Mag3(h);
        
        valid_out[i] = 255;

        cartesian_out[i3 + 0] = cartesian.x;
        cartesian_out[i3 + 1] = cartesian.y;
        cartesian_out[i3 + 2] = cartesian.z;

        cartographic_out[i2 + 0] = Rad2Deg(longitude);
        cartographic_out[i2 + 1] = Rad2Deg(latitude);
    }
    else
    {
        valid_out[i] = 0;
    }
}