#cython: language_level=3
#distutils: language = c++
import gzip
import numpy as np
cimport numpy as np
cimport cython
import pathlib
import collections
import math
import subprocess
from libc.math cimport floor
from libcpp.vector cimport vector
from cython.operator cimport dereference as deref
import sys


cdef struct TileEntry:
    unsigned long timestamp
    np.uint16_t * ptr

cdef class FastDemProvider:
    cdef long __timestamp
    cdef vector[TileEntry] __tiles
    cdef dict __objs
    cpdef object root_folder
    cpdef int capacity_tiles
    cpdef int __stat_hit
    cpdef int __stat_miss

    def __init__(self, root_folder, capacity_tiles=100):
        self.root_folder = pathlib.Path(root_folder)
        self.capacity_tiles = capacity_tiles
        self.__objs = {}
        self.__stat_hit = 0
        self.__stat_miss = 0
        self.__timestamp = 1
        cdef TileEntry tmp
        tmp.timestamp = 0
        self.__tiles.resize(181 * 361, tmp)
    
    def get_cache_stats(self):
        return f"Hit: {self.__stat_hit} Miss: {self.__stat_miss}"
    
    def get_tile(self, key):
        print("Requested tile: {}".format(key))
        path = self.root_folder.joinpath(key[:3], key + ".hgt.gz")
        if not path.exists():
            # download from the source
            print("Tile not found in local storage, downloading {}".format(key))
            subprocess.call([
                sys.executable, "-m", "awscli", "s3", "cp", "--no-sign-request",
                "s3://elevation-tiles-prod/skadi/{}/{}.hgt.gz".format(key[:3], key),
                str(path)
            ])
        data = np.frombuffer(gzip.open(path, "rb").read(), dtype=">i2").astype(np.uint16).reshape(3601, 3601)
        return data
    
    @cython.boundscheck(False)  # Deactivate bounds checking
    @cython.wraparound(False)   # Deactivate negative indexing.
    def fill_elevation_frame(self, frame):
        cdef np.uint8_t[:, ::1] valid = frame[0]
        cdef np.float64_t[:, :, ::1] cartographic = frame[2]
        cdef np.uint16_t[:, ::1] altitude = frame[3]
        cdef int y, x, ymax = cartographic.shape[0], xmax=cartographic.shape[1]
        cdef short lat_c, lon_c, lat_off, lon_off
        cdef unsigned int cache_key
        cdef np.uint16_t[:, ::1] tile

        for y in range(ymax):
            for x in range(xmax):
                if valid[y, x] != 0:
                    lat_c = <short>floor(cartographic[y, x, 0])
                    lon_c = <short>floor(cartographic[y, x, 1])
                    lat_off = <short>floor((1.0 - (cartographic[y, x, 0] - lat_c)) * 3601)
                    lon_off = <short>floor((cartographic[y, x, 1] - lon_c) * 3601)
                    cache_key = (lat_c + 90) * 361 + (lon_c + 180)
                    
                    if self.__tiles[cache_key].timestamp != 0:
                        self.__stat_hit += 1
                        # update timestamp
                        self.__tiles[cache_key].timestamp = self.__timestamp
                        self.__timestamp += 1
                        # get altitude from tile
                        altitude[y, x] = self.__tiles[cache_key].ptr[lat_off * 3601 + lon_off]
                    else:
                        self.__stat_miss += 1
                        tile_key = "%c%02d%c%03d" % ("N" if lat_c > 0 else "S", abs(lat_c), "E" if lon_c > 0 else "W", abs(lon_c))
                        tile = self.get_tile(tile_key)
                        altitude[y, x] = tile[lat_off, lon_off]

                        # save python ref to tile
                        self.__objs[cache_key] = (self.__timestamp, tile)

                        # register tile to storage
                        self.__tiles[cache_key].timestamp = self.__timestamp
                        self.__timestamp += 1
                        self.__tiles[cache_key].ptr = &tile[0, 0]
                        
                        # evict tile if necessary
                        if len(self.__objs) > self.capacity_tiles:
                            # find least recently used tile
                            cache_key = sorted(self.__objs.items(), key=lambda x: x[1][0])[0][0]
                            # mark it as evicted
                            self.__tiles[cache_key].timestamp = 0
                            self.__tiles[cache_key].ptr = <np.uint16_t *>0
                            # delete the python ref to tile
                            del self.__objs[cache_key]
