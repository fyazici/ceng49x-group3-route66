import numpy as np
import json
import pycuda.autoinit
import pycuda.driver as drv
from pycuda.compiler import SourceModule
import os
import pathlib


class EllipsoidRaytracer:
    def __init__(self, task_info, scaling=1, kernel_path=None, ellipsoid_path=None):
        if kernel_path is None:
            # default kernel and ellipsoid
            p = pathlib.Path(__file__).parent
            kernel_path = p.joinpath("depth-gen-gpu-kernel.cu")
            ellipsoid_path = p.joinpath("wgs84.json")
        self._mod = SourceModule(open(kernel_path, "r").read())
        self._pick_ellipsoid_3d_impl = self._mod.get_function(
            "pick_ellipsoid_3d_impl")
        with open(ellipsoid_path, "r") as f:
            ellipsoid = json.load(f)
            self._one_over_radii = self.read_vec3(ellipsoid["_oneOverRadii"])
            self._one_over_radii_sqr = self.read_vec3(
                ellipsoid["_oneOverRadiiSquared"])
            self._center_tol_sqr = np.float64(
                ellipsoid["_centerToleranceSquared"])
        self._width = np.int32(task_info["width"] // scaling)
        self._height = np.int32(task_info["height"] // scaling)
        aspect_ratio = np.float64(task_info["width"]) / task_info["height"]
        fov = np.float64(task_info["fov"])
        self._tan_theta = np.tan(np.deg2rad(fov)/2)
        self._tan_phi = self._tan_theta / aspect_ratio
        if "renderer_params" in task_info:
            self._near = np.float64(task_info["renderer_params"]["frustum_near"])
        else:
            # assuming Google Earth Pro uses 1 for near
            # FIXME: find more information about camera params of google earth
            self._near = np.float64(1)

    @staticmethod
    def read_vec3(p):
        return np.float64([p["x"], p["y"], p["z"]])
    
    def generate_buffers(self):
        valid_out = np.zeros((self._height, self._width), dtype=np.uint8)
        cartesian_out = np.zeros((self._height, self._width, 3), dtype=np.float64)
        cartographic_out = np.zeros((self._height, self._width, 2), dtype=np.float64)
        altitude_out = np.zeros((self._height, self._width), dtype=np.uint16)
        return (valid_out, cartesian_out, cartographic_out, altitude_out)

    def execute(self, camconf, result):            
        self._pick_ellipsoid_3d_impl(
            self._width, self._height,
            self._tan_theta, self._tan_phi, self._near,
            self.read_vec3(camconf["p"]), # positionWC
            self.read_vec3(camconf["d"]), # directionWC
            self.read_vec3(camconf["r"]), # rightWC
            self.read_vec3(camconf["u"]), # upWC
            self._one_over_radii,
            self._one_over_radii_sqr,
            self._center_tol_sqr,
            drv.Out(result[0]),
            drv.Out(result[1]),
            drv.Out(result[2]),
            block=(8, 8, 1),
            grid=(int(np.ceil(self._height/8)), int(np.ceil(self._width/8))))
