from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext
import numpy

setup(
    name="fast_dem_provider",
    ext_modules=[
        Extension("fast_dem_provider",
            sources=["fast_dem_provider.pyx"],
            language="c++",
            include_dirs=[numpy.get_include()]
        )
    ],
    cmdclass={"build_ext": build_ext}
)