import numpy as np
import json
import pycuda.autoinit
import pycuda.driver as drv
from pycuda.compiler import SourceModule
import os
import pathlib


class DepthMapper:
    def __init__(self, task_info, scaling=1, kernel_path=None, ellipsoid_path=None):
        if kernel_path is None:
            # default kernel and ellipsoid
            p = pathlib.Path(__file__).parent
            kernel_path = p.joinpath("depth-map-gpu-kernel.cu")
            ellipsoid_path = p.joinpath("wgs84.json")
        self._mod = SourceModule(open(kernel_path, "r").read())
        self._depth_map_ellipsoid_impl = self._mod.get_function(
            "depth_map_ellipsoid_impl")
        with open(ellipsoid_path, "r") as f:
            ellipsoid = json.load(f)
            self._one_over_radii_sqr = self.read_vec3(ellipsoid["_oneOverRadiiSquared"])
        self._width = np.int32(task_info["width"] // scaling)
        self._height = np.int32(task_info["height"] // scaling)
        self._logmap_zero = np.float64(task_info["renderer_params"]["logmap_zero"])

    @staticmethod
    def read_vec3(p):
        return np.float64([p["x"], p["y"], p["z"]])
    
    def generate_buffers(self):
        depth_u8 = np.zeros((self._height, self._width), dtype=np.uint8)
        altitude_u8 = np.zeros((self._height, self._width), dtype=np.uint8)
        return (depth_u8, altitude_u8)

    def execute(self, camconf, in_result, out_result):  
        self._depth_map_ellipsoid_impl(
            self._width, self._height,
            self.read_vec3(camconf["p"]), # positionWC
            self._one_over_radii_sqr,
            self._logmap_zero,
            drv.In(in_result[0]),
            drv.In(in_result[1]),
            drv.In(in_result[3]),
            drv.Out(out_result[0]),
            drv.Out(out_result[1]),
            block=(8, 8, 1),
            grid=(int(np.ceil(self._height/8)), int(np.ceil(self._width/8))))
