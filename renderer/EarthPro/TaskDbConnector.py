#!/usr/bin/env python3
#HD m4V High C:/Users/Egemen/Desktop/ resultVideo OnePass.txt
# Mockup class for development

class TaskDbConnector:
    def __init__(self):
        pass

    def connect(self):
        pass

    def get_task(self, task_id):
        if task_id == 1:
            return {
                "resolution": "HD",
                "fileType": "m4V",
                "pictureQuality": "High",
                "videoName": "Result",
                "missionLogName": "OnePass.txt",
            }
        elif task_id == 2:
            return {
                "resolution": "HD",
                "fileType": "m4V",
                "pictureQuality": "High",
                "videoName": "Result",
                "missionLogName": "FullTour.txt",
            }
        
