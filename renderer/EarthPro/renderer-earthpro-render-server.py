#!/usr/bin/env python3

from flask import Flask, request, jsonify, send_file
from flask_cors import CORS
from flask_sockets import Sockets
import subprocess
import sys
import datetime
import time
from TaskDbConnector import TaskDbConnector
import os

app = Flask(__name__, static_url_path="/static", static_folder="static")
CORS(app)
sockets = Sockets(app)

eartGui_proc = None
taskdb_conn = None
current_task = None


ABSPATH = os.path.dirname(os.path.abspath(__file__)) + "\\"

@app.route("/start", methods=["POST"])
def start():
    global eartGui_proc, taskdb_conn, current_task, browser_proc

    #Temporarly used dummy database
    task_id = request.json["task_id"]
    current_task = taskdb_conn.get_task(task_id)

    earthGui_cmd = ["python", "renderer_AutoGuiEnhanced.py "]
    earthGui_cmd += [current_task["resolution"]]
    earthGui_cmd += [current_task["fileType"]]
    earthGui_cmd += [current_task["pictureQuality"]]
    earthGui_cmd += [ABSPATH]
    earthGui_cmd += [current_task["videoName"]]
    earthGui_cmd += [current_task["missionLogName"]]
    
    eartGui_proc = subprocess.Popen(earthGui_cmd, stdin=subprocess.PIPE)
    return jsonify(success=True)

@app.route("/mission-log", methods=["GET"])
def get_mission_log():
    global current_task
    return send_file(current_task["missionlog_path"],
        mimetype="text/plain",
        as_attachment=True, cache_timeout=0)

@app.route("/task-info", methods=["GET"])
def get_task_info():
    global current_task
    return current_task

if __name__ == '__main__':
    from gevent import pywsgi
    from geventwebsocket.handler import WebSocketHandler

    taskdb_conn = TaskDbConnector()
    taskdb_conn.connect()

    server = pywsgi.WSGIServer(("", 5001), app, handler_class=WebSocketHandler)
    server.serve_forever()
