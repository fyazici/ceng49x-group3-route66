#!/usr/bin/env python3

# Mockup class for development
class TaskDbConnector:
    def __init__(self):
        pass

    def connect(self):
        pass

    def get_task(self, task_id):
        if task_id == 1:
            return {
                "width": 1920,
                "height": 1080,
                "fov": 90,
                "framerate": 30,
                "camera_pan": 0,
                "camera_tilt": -20,
                "camera_roll": 0,
                "missionlog_path": "OnePass.txt",
                "samplerate": 100,
                "time_offset": 0,
                "renderer_params": {
                    "renderer": "cesiumjs", # alternative: "earthpro"
                    "skybox": 1,
                    "lighting_mode": 1,
                    "light_fade_dist": 0,
                    "night_fade_dist": 0,
                    "shadows": 1,
                    "terrain_shadow_mode": 3,
                    "show_ground_atm_mode": 1,
                    "terrain_depth_test": 1,
                    "time_advance_mode": 0,
                    "water_mask": 1,        # currently not configurable
                    "vertex_normals": 1,    # currently not configurable
                    "frustum_near": 1.0,
                    "frustum_far": 1e+8,
                },
                "output_path": "OnePass.mp4",
                # pix_fmt and faststart is important for streamable video output
                "encoding_params": [
                    "-profile:v", "high",
                    "-pix_fmt", "yuv420p",
                    "-movflags", "faststart"
                ]
            }
        elif task_id == 2:
            return {
                "width": 800,
                "height": 450,
                "fov": 90,
                "framerate": 30,
                "camera_pan": 0,
                "camera_tilt": -20,
                "camera_roll": 0,
                "missionlog_path": "FullTour.txt",
                "samplerate": 100,
                "time_offset": "Fri Feb 07 2020 02:30:59 GMT+0300 (GMT+03:00)", # any JS date format works
                "renderer_params": {
                    "renderer": "cesiumjs", # alternative: "earthpro"
                    "skybox": 1,
                    "lighting_mode": 1,
                    "light_fade_dist": 0,
                    "night_fade_dist": 0,
                    "shadows": 1,
                    "terrain_shadow_mode": 1,   # terrain only casts shadows, does not receive it
                    "show_ground_atm_mode": 0,  # on linux radeon gpu driver, enabling this causes white patches bug
                    "terrain_depth_test": 1,
                    "time_advance_mode": 1,     # time (position of earth in sun orbit) changes every frame by samplerate period
                    "water_mask": 1,        # currently not configurable
                    "vertex_normals": 1,    # currently not configurable
                    "frustum_near": 1.0,    
                    "frustum_far": 1e+8,
                },
                "output_path": "FullTour.mp4",
                "encoding_params": ["-profile:v", "high", "-pix_fmt", "yuv420p", "-movflags", "faststart"]
            }
        
