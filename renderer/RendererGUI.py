from tkinter import *
from tkinter.filedialog import *
from tkinter.simpledialog import *
from tkinter.ttk import Combobox
from PIL import ImageTk, Image
from pathlib import Path
import subprocess
#import imageio can be used later
import os
import cv2
class GUI:
    googleEarhProParameters = {"Resolution": {"QVGA" : "QVGA (320×240 pixels, 30 frames/sec)",
                                              "VGA" :  "VGA (640×480 pixels, 30 frames/sec)",
                                              "SVGA" : "SVGA (800×600 pixels, 30 frames/sec)",
                                              "XGA" :  "XGA (1024×768 pixels, 30 frames/sec)",
                                              "HD" :   "HD 720p (1280×720 pixels, 60 frames/sec)",
                                              "FHD" :  "HD 1080p (1920×1080 pixels, 60 frames/sec)",
                                              "UHD" :  "UHD 2160p (3840×2160 pixels, 30 frames/sec)"},
                                "File Type": {"m4V" : "H.264 (m.4v)",
                                              "VP8" : "VP8 (.webm)",
                                              "VP9" : "VP9 (.webm)",
                                              "VP9 Lossless" : "VP9 lossless (.webm)",
                                              "ASF" : "Windows Media (.asf)",
                                              "MP4" : "MJPEG (.mp4)",
                                              "JPG" : "JPEG image sequence (.jpg)",
                                              "PNG" : "PNG image sequence (.png)"},
                                "Picture Quality": {"Minumum" : "Minumum",
                                              "Low" : "Low",
                                              "Medium" : "Medium",
                                              "High" : "High",
                                              "Maximum" : "Maximum"}}

    renderers = {"Google Earth Pro" : googleEarhProParameters, "CesiumJS" : "2"}

    def __init__(self, parent):

        self.parent = parent
        self.createTopNavigationBar()
        self.activeRenderer = GUI.renderers["Google Earth Pro"]
        self.stopped = False

    def createTopNavigationBar(self):
        
        self.topFrame = Frame(self.parent)
        self.topFrame.pack(side = TOP, fill = BOTH)

        self.parameterFrame = Frame(self.parent)
        self.parameterFrame.pack(side = TOP)

        self.rendererSelectButton_EarthPro = Button(self.topFrame, text = "Google Earth Pro", command = (lambda renderer = "Google Earth Pro" : self.activate(renderer)))
        self.rendererSelectButton_CesiumJS = Button(self.topFrame, text = "CesiumJS", command = (lambda renderer = "CesiumJS" : self.activate(renderer)))
        self.renderersButtons = {"Google Earth Pro" : self.rendererSelectButton_EarthPro, "CesiumJS" : self.rendererSelectButton_CesiumJS}
        
        self.rendererSelectButton_EarthPro.pack(side = LEFT, fill = BOTH, expand = YES)
        self.rendererSelectButton_CesiumJS.pack(side = RIGHT, fill = BOTH, expand = YES)

        self.activate("Google Earth Pro")

    def activate(self, renderer):
        
        self.parameterFrame.destroy()
        self.parameterFrame = Frame(self.parent)
        self.parameterFrame.pack(side = TOP, fill = BOTH)

        self.videoFrame = Frame(self.parameterFrame)
        self.videoFrame.pack(side = RIGHT, expand = YES, fill = BOTH)

        self.renderersButtons[renderer].config(bg = "green")

        for rendererName, rendererButton in GUI.renderers.items():
            if(renderer != rendererName):
                self.renderersButtons[rendererName].config(bg = "red")

        self.createParameterForRenderer(renderer)
            
    def createParameterForRenderer(self, renderer):

        temporaryFrame = Frame(self.parameterFrame)
        temporaryFrame.pack(side = TOP, fill = "x")
        Label(temporaryFrame, text = "Mission Log File : \t").pack(side = LEFT)
        Button(temporaryFrame, command = self.getMissionLogFile, text = "Select File").pack(side = LEFT)

        self.comboBoxes = []

        for parameterName, parameterTypes in GUI.renderers[renderer].items():

            temporaryFrame = Frame(self.parameterFrame)
            temporaryFrame.pack(side = TOP, fill = "x")

            Label(temporaryFrame, text = parameterName + " : \t", anchor='w').pack(pady = 5, side = LEFT, fill = "both")
            tempBox = Combobox(temporaryFrame, values = list(parameterTypes.values()), width = 45)
            tempBox.pack(side = LEFT)
            self.comboBoxes.append(tempBox)
        
        temporaryFrame = Frame(self.parameterFrame)
        temporaryFrame.pack(side = TOP, fill = "x")
        Label(temporaryFrame, text = "Output Destination :").pack(side = LEFT)
        Button(temporaryFrame, command = self.getOutputDestination, text = "Select Path").pack(side = LEFT)

        temporaryFrame = Frame(self.parameterFrame)
        temporaryFrame.pack(side = TOP, fill = "x")
        Button(temporaryFrame, command = self.startRendering, text = "Render",bg = "red").pack(side = LEFT,pady = 10)        

        temporaryFrame = Frame(self.parameterFrame)
        temporaryFrame.pack(side = TOP, fill = "x")
        Button(temporaryFrame, command = self.generateVideoFrame, text = "Display",bg = "green").pack(side = LEFT,pady = 10)        


    def getMissionLogFile(self):

        self.missionLogFile = askopenfilename()

    def getOutputDestination(self):

        self.outputDestination = askdirectory()

    def getFrame(self):
        if self.video.isOpened():
            ret,frame = self.video.read()
            if ret:
                return (ret, cv2.cvtColor(frame, cv2.COLOR_BGR2RGB))

    def generateVideoFrame(self):
        self.videoLabel = Label(self.videoFrame)
        self.videoLabel.pack()
##        will be implemented later
##        self.pauseButton = Button(self.videoFrame, text = "Pause", command = self.pause)
##        self.pauseButton.pack(side = BOTTOM)
##        backwardButton = Button(self.videoFrame, text = "Backward", command = self.backward)
##        backwardButton.pack(side = BOTTOM)
        
        self.video = cv2.VideoCapture(askopenfilename())
        if not self.video.isOpened():
            raise ValueError("Video is not avaliable")
        self.width = self.video.get(cv2.CAP_PROP_FRAME_WIDTH)
        self.height = self.video.get(cv2.CAP_PROP_FRAME_HEIGHT)
        self.videoLabel.after(15, lambda: self.updateLabel())
    def updateLabel(self):
        ret, frame = self.getFrame()
        if ret:
            frameImage = ImageTk.PhotoImage(image = Image.fromarray(frame))
            self.videoLabel.config(image = frameImage)
            self.videoLabel.image = frameImage
            self.videoLabel.after(15, lambda: self.updateLabel())
    def startRendering(self):
        videoName = askstring("Video Name", "Enter video name",
                                parent=self.parent)
        command = "renderer_AutoGuiEnhanced.py "
        for i,box in enumerate(self.comboBoxes):
            parameter = list(GUI.googleEarhProParameters)[i]
            for key,item in GUI.googleEarhProParameters[parameter].items():
                if item == box.get():
                    command += key + " "
        command += self.outputDestination + "/ "
        command += videoName + " "
        command += Path(self.missionLogFile).name

        self.rendering = subprocess.Popen('cmd /c'+ command,shell=False)

##        self.video = imageio.get_reader("C:/Users/Egemen/Desktop/Bitirme/resultVideo.m4v")
##        self.videoLabel.pack()
##        self.videoLabel.after(int(1000 / self.video.get_meta_data()['fps']), lambda: self.stream())


##    def stream(self):
##        if(self.stopped):
##            return
##        try:
##            image = self.video.get_next_data()
##        except:
##            self.video.close()
##            return
##        
##        self.videoLabel.after(int(1000 / self.video.get_meta_data()['fps']), lambda: self.stream())
##        frame_image = ImageTk.PhotoImage(Image.fromarray(image))
##        self.videoLabel.config(image = frame_image)
##        self.videoLabel.image = frame_image
##
##    def backward(self):
##
##        if(self.stopped):
##            return
##        
##        try:
##            image = self.video.get_previous_data()
##            
##        except:
##            self.video.close()
##
##        self.videoLabel.after(int(1000 / self.video.get_meta_data()['fps']), lambda: self.backward())
##        frame_image = ImageTk.PhotoImage(Image.fromarray(image))
##        self.videoLabel.config(image = frame_image)
##        self.videoLabel.image = frame_image
##
##    def pause(self):
##        self.stopped = True
##        self.pauseButton["text"] = "Continue"
##        self.pauseButton["command"] = (lambda: self.play())
##
##    def play(self):
##        self.stopped = False
##        self.pauseButton["text"] = "Pause"
##        self.pauseButton["command"] = (lambda: self.pause())
##        self.videoLabel.after(int(1000 / self.video.get_meta_data()['fps']), lambda: self.stream())
##
    def closeVideo(self):
        try:
            self.video.release()
        except:
            pass
        self.parent.destroy()
if __name__ == "__main__":

    root = Tk()
    root.title("Route66")

    gui = GUI(root)
    root.protocol("WM_DELETE_WINDOW", gui.closeVideo)
    root.mainloop()
