import cv2
from modules.Perlin import *
from modules.Sample import *
from modules.FogFactor import *
import subprocess
import pathlib
import os
import argparse
import json

try:
    FFMPEG_EXECUTABLE = os.environ["R66_FFMPEG_EXECUTABLE"]
except KeyError:
    FFMPEG_EXECUTABLE = r"ffmpeg"

#normalizes depth to 0-1
def normalize_depth(depth, min_dist=0, max_dist=100, min_val=0, max_val=1):
    temp=np.copy(depth)
    temp[temp>max_dist]=max_dist
    temp-=min_dist
    return temp/max_dist

#converts depth to rgb image so that it can be displayed
def depth2rgb(depth):    
    img=normalize_depth(depth)
    stacked_img = np.stack((img,)*3, axis=-1)
    stacked_img=np.apply_along_axis(lambda x:(255,0,0) if x[0]==np.inf else x ,axis=2,arr=stacked_img)
    return stacked_img


#to render whole video
def render_video(task_info, org_path, depth_path, valid_path, output_dir):
    options = task_info['renderer_params']['options']
    cap_depth = cv2.VideoCapture(depth_path)
    cap_org=cv2.VideoCapture(org_path)
    cap_valid=cv2.VideoCapture(valid_path)
    if (cap_depth.isOpened()== False or cap_org.isOpened()==False or cap_valid.isOpened()==False): 
        print('error')
        return
    
    min_dist=options.get("min_dist", 0)
    max_dist=options.get("max_dist", 100)
    if('perlin' in options):
        perlin_k=options['perlin'].get("k", 0.3)
        perlin_octave=options['perlin'].get("octave", 5)
        perlin_frequency=options.get("perlin_frequency",[5,5,1])
        is_perlin=True
        p=PerlinNoiseFactory(3,octaves=perlin_octave)
        s=SamplePerlinFog(p,(int(cap_depth.get(3)), int(cap_depth.get(4))), perlin_frequency)

    else: 
        is_perlin=False
    if options['fog_factor']['type']=='exponential':
        fg=ExpFogGenerator(options['fog_factor'].get("b", 1.5))
    elif options['fog_factor']['type']=='exponential2':
        fg=ExpFogGenerator2(options['fog_factor'].get("b", 1.5))
    elif options['fog_factor']['type']=='linear':
        fg=LinFogGenerator(0, 1)  
    
    fog_color=options.get("fog_color",[240,225,225])
    max_density=options.get("max_density",1)
    #out = cv2.VideoWriter('outpy_perlin.avi',cv2.VideoWriter_fourcc('M','J','P','G'), 25, (1920,1080))
    ffmpeg_proc = subprocess.Popen([
            FFMPEG_EXECUTABLE, "-v", "warning", "-stats", "-y", "-f", "rawvideo","-pixel_format", "bgr24",
            "-framerate", str(task_info["framerate"]),
            '-s', '{}x{}'.format(task_info['width'], task_info['height']),
            "-i", "-", "-profile:v", "high", "-pix_fmt", "yuv420p", "-movflags", "faststart",
           str(pathlib.Path(output_dir).joinpath("fog.mp4"))
        ], stdin=subprocess.PIPE)

    #ffmpeg_proc.stdin.close()
    #ffmpeg_proc.wait()
    i=0
    while cap_org.isOpened() and cap_depth.isOpened():
        i+=1
        #print(i)
        _,frame_depth=cap_depth.read()
        _,frame_org=cap_org.read()
        _,frame_valid=cap_valid.read()
        if frame_valid is None or frame_valid is None or frame_valid is None: break
        frame_depth=frame_depth[:,:,0]
        frame_valid=frame_valid[:,:,0]
        #frame_valid[frame_valid<=127]=0
        #frame_valid[frame_valid>127]=1
        non_valid_depth=np.where(frame_valid<=127)
        #non_valid_depth=np.where(non_valid_depth==0)
        logmap_zero = 100.0
        if "logmap_zero" in task_info["renderer_params"]:
            logmap_zero = task_info["renderer_params"]["logmap_zero"]
        depth = (2 ** (frame_depth / 16)) * logmap_zero
        #depth=(frame_depth[:,:,1]+((frame_depth[:,:,2]).astype(np.uint16))/16 ).astype(np.float32)
        depth[non_valid_depth]=np.inf
        fog_matrix=fg.calc_fog_factor(normalize_depth(depth,min_dist,max_dist))
        if(is_perlin):fog_matrix=fog_matrix + s(i/150)*perlin_k

        #clamp to 0-1
        fog_matrix[fog_matrix<1-max_density]=1-max_density
        fog_matrix[fog_matrix>1]=1

        #resize fog matrix to orginal image size by interpolating
        fog_matrix=cv2.resize(fog_matrix, dsize=(int(cap_depth.get(3)), int(cap_depth.get(4))), interpolation=cv2.INTER_CUBIC)

        stacked_img = np.stack((fog_matrix,)*3, axis=-1)
        x=np.multiply(stacked_img,frame_org)+(1-stacked_img)*fog_color
        #out.write(np.uint8(x))
    
        ffmpeg_proc.stdin.write(np.uint8(x).tostring())
    ffmpeg_proc.stdin.close()
    ffmpeg_proc.wait()
    #ffmpeg_proc.kill()
    #out.release()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--task-info", "-t")
    parser.add_argument("--original-path", "-s")
    parser.add_argument("--depth-path", "-d")
    parser.add_argument("--valid-path", "-v")
    parser.add_argument("--output-dir", "-o")
    args = parser.parse_args()

    render_video(
        json.loads(args.task_info), 
        args.original_path, 
        args.depth_path, 
        args.valid_path,
        args.output_dir)