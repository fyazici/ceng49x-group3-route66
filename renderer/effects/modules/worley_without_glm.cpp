#include<stdlib.h>
#include<stdio.h>
#include "vec3.cpp"
#define MOD3 vec3(.1031,.11369,.13787)
vec3 hash33(vec3 p3)
{
    p3 = fract(p3 * MOD3);
    p3 += dot(p3, vec3(p3.y+19.19,p3.x+19.19,p3.z+19.19));
    return fract(vec3((p3.x + p3.y)*p3.z, (p3.x+p3.z)*p3.y, (p3.y+p3.z)*p3.x)) * 2.0f - 1.0f;
}

// min() is not available
template<class T> 
const T& min(const T& a, const T& b) { return (b < a) ? b : a; }

extern "C"
#ifdef _MSC_VER
__declspec(dllexport)
#endif 
float remap(float x, float a, float b, float c, float d)
{
    return (((x - a) / (b - a)) * (d - c)) + c;
}

// Tileable 3D worley noise
float worleyNoise(vec3 uv, float freq)
{    
    vec3 id = floor(uv);
    vec3 p = fract(uv);
    
    float minDist = 10000.;
    for (float x = -1.; x <= 1.; ++x)
    {
        for(float y = -1.; y <= 1.; ++y)
        {
            for(float z = -1.; z <= 1.; ++z)
            {
                vec3 offset = vec3(x, y, z);
                vec3 h = hash33(mod(id + offset, vec3(freq))) * .4 + .3f; // [.3, .7]
                h += offset;
                vec3 d = p - h;
                minDist = min(minDist, dot(d, d));
            }
        }
    }
    
    // inverted worley noise
    return 1. - minDist;
}

extern "C" 
#ifdef _MSC_VER
__declspec(dllexport)
#endif
float worleyFbm(float x, float y, float z, float freq)
{
    vec3 p=vec3(x,y,z);
    
    return worleyNoise(p*freq, freq) * .625 +
             worleyNoise(p*freq*2., freq*2.) * .25 +
             worleyNoise(p*freq*4., freq*4.) * .125;
}

/*
int main(int argC,char* argV[])
{

    vec3 x=hash33(vec3(0.2,0.1,0.4));
    vec3 y=x+.1f;
    printf("%f\n", x.x);
    return 0;
}
*/
//g++ -shared -std=c++11 -lXi -lGLEW -lGLU -lm -lGL -I./ -o worley_without_glm.so -fPIC worley_without_glm.cpp
