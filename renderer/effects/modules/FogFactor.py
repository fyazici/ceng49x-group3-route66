import numpy as np

class ExpFogGenerator:
    def __init__(self,b):
        self.b=b
    def calc_fog_factor(self,distance):
        return 1/np.exp((distance*self.b)**2)
    def calc_fog_factor_2(self,distance):
        return 1/np.exp(distance*self.b)
    def calc_fog_factor_alt(self,distance,altitude_fog_start, altitude_fog_end):
        return 1/np.exp(distance*self.b)

class ExpFogGenerator2:
    def __init__(self,b):
        self.b=b
    def calc_fog_factor(self,distance):
        return 1/np.exp(distance*self.b)
    
    
class ExpFogGeneratorWithAltitude:
    def __init__(self,b,altitude_fog_start, altitude_fog_end):
        self.b=b
        self.altitude_fog_start=altitude_fog_start
        self.altitude_fog_end=altitude_fog_end

    def calc_fog_factor(self,distance,altitude):
    	#calc contribution from distance
        fog_factor_from_distance=1/np.exp(distance*self.b)

        #calc contribution from altitude
        fog_factor_from_altitude=(self.altitude_fog_end-altitude)/(self.altitude_fog_end-self.altitude_fog_start)
        
        #calc average of both
        return (fog_factor_from_distance+fog_factor_from_altitude)/2

class LinFogGenerator:
    def __init__(self,fog_start,fog_end):
        self.fog_end=fog_end
        self.fog_start=fog_start
    
    def calc_fog_factor(self,distance):
        return (self.fog_end-distance)/(self.fog_end-self.fog_start)