import subprocess
import sys
import shutil

if __name__ == "__main__":
    if sys.platform.startswith("win"):
        try:
            shutil.copyfile("f_perlin.dll", "f_perlin.shared")
            shutil.copyfile("worley_without_glm.dll", "worley_without_glm.shared")
        except Exception as e:
            print(e)
        # subprocess.call(["cl.exe", "/LD", "/Ox", "/EHsc", "f_perlin.shared"])
        # subprocess.call(["cl.exe", "/LD", "/Ox", "/EHsc", "worley_without_glm.shared"])
    else:
        subprocess.call(["g++", "-shared", "-std=c++11", "-I./", "-o", "f_perlin.shared", "-fPIC", "f_perlin.c"])
        subprocess.call(["g++", "-shared", "-std=c++11", "-I./", "-o", "worley_without_glm.shared", "-fPIC", "worley_without_glm.cpp"])