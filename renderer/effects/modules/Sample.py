import cv2
import numpy as np
class SamplePerlinFog:
	def __init__(self, perlin_factory, size, frequency=None):
		self.f=frequency if frequency is not None else [1,1]
		self.perlin_factory=perlin_factory
		self.size=size
		self.sample_size=(250,250)
		self.sample=[[0 for i in range(self.sample_size[0])] for j in range(self.sample_size[1])]

	def __call__(self, *point):
		if len(self.f)!=len(point)+2:
			print("frequency dimension number is not same as perlin dimension number defaulting to freq=1")
			self.f=[1]*(len(point)+2)
		if len(point)+2 != self.perlin_factory.dimension:
			raise ValueError("Expected {} values, got {}".format(
                self.perlin_factory.dimension, len(point)))

		for i in range(self.sample_size[0]):
			for j in range(self.sample_size[1]):
				self.sample[i][j]=self.perlin_factory(self.f[0]*(i/self.sample_size[0]),self.f[1]*(j/self.sample_size[1]), *[self.f[i+2]*p for i,p in enumerate(point)])

		return cv2.resize(np.array(self.sample), dsize=self.size, interpolation=cv2.INTER_CUBIC)