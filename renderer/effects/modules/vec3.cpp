#include<stdio.h>
#include "iostream"
#include <math.h> 
using namespace std;
class vec3 {
    public:
        vec3();
        vec3(float, float, float);
        vec3(float);
        bool operator==(vec3 rhs);
        vec3 operator+(vec3 rhs);
        vec3 operator+(float rhs);
        vec3& operator+=(vec3 rhs);
        vec3& operator+=(float rhs);
        vec3 operator-(vec3 rhs);
        vec3 operator-(float rhs);
        vec3 operator*(float scalar);
        vec3 operator*(vec3 rhs);
        vec3 operator/(float scalar);
        vec3 cross(vec3 rhs);
        float dot(vec3 rhs);
        float length();

        float x;
        float y;
        float z;

};
vec3::vec3(float x_, float y_, float z_)
{
    this->x = x_;
    this->y = y_;
    this->z = z_;
}
vec3::vec3(float x_)
{
    this->x = x_;
    this->y = x_;
    this->z = x_;
}
vec3::vec3()
{
    this->x = 0;
    this->y = 0;
    this->z = 0;
}
bool vec3::operator==(vec3 rhs) {
    return(x == rhs.x && y == rhs.y && z == rhs.z);
}
vec3 vec3::operator-(vec3 rhs) {
    return vec3( x - rhs.x, 
                 y - rhs.y, 
                 z - rhs.z);
}
vec3 vec3::operator-(float rhs) {
    return vec3( x - rhs, 
                 y - rhs, 
                 z - rhs);
}
vec3 vec3::operator+(vec3 rhs) {
    return vec3( x + rhs.x, 
                 y + rhs.y, 
                 z + rhs.z);
}
vec3 vec3::operator+(float rhs) {
    return vec3( x + rhs, 
                 y + rhs, 
                 z + rhs);
}
vec3& vec3::operator+=(vec3 rhs) {
    this->x+=rhs.x;
    this->y+=rhs.y;
    this->z+=rhs.z;
    return *this;
}
vec3& vec3::operator+=(float rhs) {
    this->x+=rhs;
    this->y+=rhs;
    this->z+=rhs;
    return *this;
}
vec3 vec3::operator/(float scalar) {
    return vec3(x / scalar, 
                y / scalar,
                z / scalar);
}

float vec3::dot(vec3 rhs) {
    return (x * rhs.x + 
            y * rhs.y + 
            z * rhs.z);
}
float dot(vec3 lhs, vec3 rhs) {
    return lhs.dot(rhs);
}
vec3 vec3::cross(vec3 rhs) {
    return vec3( y * rhs.z - z * rhs.y,
                 z * rhs.x - x * rhs.z,
                 x * rhs.y - y * rhs.x);
}
vec3 vec3::operator*(vec3 rhs) {
    return (this->cross(rhs));
}
vec3 vec3::operator*(float rhs) {
    return vec3( x*rhs,y*rhs,z*rhs );
}
vec3 fract(vec3 inp){
    return vec3(inp.x-(long)inp.x, inp.y-(long)inp.y, inp.z-(long)inp.z);
}
vec3 floor(vec3 inp){
    return vec3((long)inp.x, (long)inp.y, (long)inp.z);
}
vec3 mod(vec3 inp1, vec3 inp2){
    vec3 t=vec3(inp1.x-floor(inp1.x/inp2.x)*inp2.x,
                inp1.y-floor(inp1.y/inp2.y)*inp2.y,
                inp1.z-floor(inp1.z/inp2.z)*inp2.z);
    return t;
}
/*
int main(int argc, char const *argv[])
{

    vec3 x=vec3(1.1, 2.4, 3.4);
    cout << (x*2).x << ":" << (x*2).y << endl;
    return 0;
}*/