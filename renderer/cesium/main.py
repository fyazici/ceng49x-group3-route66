import argparse
import datetime
import json
import os
import subprocess
import time
import sys
import pathlib

from flask import Flask, jsonify, request, send_file
from flask_cors import CORS
from flask_sockets import Sockets
from gevent import pywsgi
from gevent.event import Event
from geventwebsocket.handler import WebSocketHandler

app = Flask(__name__, static_url_path="/static", static_folder="static")
app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 0
CORS(app)
sockets = Sockets(app)

try:
    RENDER_PORT = int(os.environ["R66_RENDER_PORT"])
except:
    RENDER_PORT = 5002
RENDER_URL = "http://127.0.0.1:" + str(RENDER_PORT)

try:
    FIREFOX_EXECUTABLE = os.environ["R66_FIREFOX_EXECUTABLE"]
except KeyError:
    if sys.platform.startswith("win"):
        FIREFOX_EXECUTABLE = r"C:\Program Files\Mozilla Firefox\firefox.exe"
    else:
        FIREFOX_EXECUTABLE = r"firefox"

try:
    FFMPEG_EXECUTABLE = os.environ["R66_FFMPEG_EXECUTABLE"]
except KeyError:
    FFMPEG_EXECUTABLE = r"ffmpeg"
    
BROWSER_CMD = [FIREFOX_EXECUTABLE, "-no-remote", "-profile", "./headless_profiles/headless_{}".format(RENDER_PORT), RENDER_URL + "/static/cesium-renderer.html"]

ffmpeg_proc = None
current_task = None
browser_proc = None
camera_conf_file_stream = None
frame_count = None
stopper = None
missionlog_path = None
output_dir = None


def render_start():
    global frame_count, camera_conf_file_stream, current_task, output_dir, ffmpeg_proc, browser_proc
    frame_count = 0

    # NOTE: .jsonS denotes that it is not a valid json file
    # it is a list of valid one-liner json objects for each frames camera configuration
    camera_conf_file_stream = open(pathlib.Path(output_dir).joinpath("camconf.jsons"), "w+")

    if current_task["renderer_params"]["dry_run"] == 0:
        ffmpeg_cmd = [FFMPEG_EXECUTABLE, "-v", "warning", "-stats", "-y", "-f", "image2pipe"]
        ffmpeg_cmd += ["-framerate", str(current_task["framerate"])]
        ffmpeg_cmd += ["-i", "-"]
        ffmpeg_cmd += current_task["encoding_params"]
        ffmpeg_cmd += [str(pathlib.Path(output_dir).joinpath("cesium.mp4"))]
        ffmpeg_proc = subprocess.Popen(ffmpeg_cmd, stdin=subprocess.PIPE)
    
    nvidia_env = os.environ.copy()
    if sys.platform.startswith("win"):
        nvidia_env["SHIM_MCCOMPAT"] = "0x800000001"
    elif sys.platform.startswith("linux"):
        nvidia_env["__NV_PRIME_RENDER_OFFLOAD"] = "1"
        nvidia_env["__GLX_VENDOR_LIBRARY_NAME"] = "nvidia"
    browser_proc = subprocess.Popen(BROWSER_CMD, env=nvidia_env)


def render_join():
    global ffmpeg_proc, camera_conf_file_stream, browser_proc, current_task
    browser_proc.wait()
    if current_task["renderer_params"]["dry_run"] == 0:
        ffmpeg_proc.stdin.close()
        ffmpeg_proc.wait()
    camera_conf_file_stream.close()


@app.route("/mission-log", methods=["GET"])
def flask_mission_log():
    global missionlog_path
    return send_file(missionlog_path, mimetype="text/plain", as_attachment=True, cache_timeout=0)


@app.route("/task-info", methods=["GET"])
def flask_task_info():
    global current_task
    return current_task


@app.route("/encode-end", methods=["POST"])
def flask_encode_end():
    global stopper
    stopper.set()
    return jsonify(success=True)


@sockets.route("/encode")
def socket_encode(ws):
    global frame_count
    last_time = time.time()
    while not ws.closed:
        global ffmpeg_proc
        message = ws.receive()
        if message is not None:
            if time.time() - last_time > 1:
                last_time = time.time()
                with open(pathlib.Path(output_dir).joinpath("thumb.jpg"), "wb+") as f:
                    f.write(message)
            ffmpeg_proc.stdin.write(message)
            frame_count += 1
            if not ws.closed:
                ws.send("next")


@sockets.route("/camera-conf")
def socket_camera_conf(ws):
    global camera_conf_file_stream
    while not ws.closed:
        conf = ws.receive()
        if conf is not None:
            camera_conf_file_stream.write(conf + "\n")
            if not ws.closed:
                ws.send("next")


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--task-info", "-t")
    parser.add_argument("--missionlog-path", "-m")
    parser.add_argument("--output-dir", "-o")
    args = parser.parse_args()

    current_task = json.loads(args.task_info)
    missionlog_path = args.missionlog_path
    output_dir = args.output_dir
    stopper = Event()

    server = pywsgi.WSGIServer(("127.0.0.1", RENDER_PORT), app, handler_class=WebSocketHandler)
    server.start()
    render_start()
    stopper.wait()
    render_join()
