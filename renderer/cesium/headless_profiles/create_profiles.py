import shutil
import argparse

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("port_min", nargs="?", type=int, default=5002)
    parser.add_argument("port_max", nargs="?", type=int, default=5012)
    args = parser.parse_args()

    for port in range(args.port_min, args.port_max):
        try:
            shutil.rmtree("headless_{}".format(port))
        except:
            pass
        shutil.copytree("headless_default", "headless_{}".format(port))
