import subprocess
import json
import os

task_info = {
    'camera_pan': 0.0,
    'camera_roll': 0.0,
    'camera_tilt': -20.0,
    'encoding_params': ['-movflags', 'faststart', '-c:v', 'h264_nvenc'],
    'fov': 90.0,
    'framerate': 30,
    'missionlog_path': './log-file/OnePass.txt',
    'name': None,
    'output_path': 'work/task2.mp4',
    'renderer_params': {
        'dry_run': 0,
        'frustum_far': 100000000,
        'frustum_near': 1.0,
        'light_fade_dist': 10000000,
        'lighting_mode': 1,
        'night_fade_dist': 10000000,
        'renderer': 'cesiumjs',
        'shadows': 1,
        'show_ground_atm_mode': 1,
        'skybox': 1,
        'terrain_depth_test': 1,
        'terrain_shadow_mode': 3,
        'time_advance_mode': 0,
        'vertex_normals': 1,
        'water_mask': 1
    },
    'samplerate': 100,
    'task_id': 2,
    'user_id': 1,
    'width': 1920,
    'height': 1080
}

missionlog_path = "work/OnePass.txt"

# call from two dirs above
cesium_env = os.environ.copy()
cesium_env["R66_PORT"] = "5002"
cesium_proc = subprocess.Popen(
    ["python3", "main.py", "-t", json.dumps(task_info), "-m", missionlog_path], 
    env=cesium_env, cwd="cesium")
cesium_proc.wait()
