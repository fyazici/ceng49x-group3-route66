import argparse
import json
import subprocess
from pywinauto import *
from pywinauto.keyboard import *
import util
from util.localization import en_us as _t
import threading
import pathlib
import time
import pygetwindow
import os
import sys


if sys.platform.startswith("win"):
    EARTHPRO_EXECUTABLE = r"C:\Program Files\Google\Google Earth Pro\client\googleearth.exe"
else:
    EARTHPRO_EXECUTABLE = r"googleearth"

try:
    FFMPEG_EXECUTABLE = os.environ["R66_FFMPEG_EXECUTABLE"]
except KeyError:
    FFMPEG_EXECUTABLE = r"ffmpeg"

def get_screenshot(win, output_dir):
    while True:
        try:
            img = win["Pane9"].capture_as_image()
            img.save(str(pathlib.Path(output_dir).joinpath("thumb.jpg")))
            time.sleep(1)
        except:
            break


def render(task_info, missionlog_path, output_dir):
    kml_path = str(pathlib.Path(output_dir).joinpath("mission.kml"))
    util.convert_kml(task_info, missionlog_path, kml_path)

    earth_proc = subprocess.Popen([EARTHPRO_EXECUTABLE, kml_path],
                                  stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)

    # poll until google earth window could be found
    while True:
        time.sleep(3)
        try:
            earth_app = Application(backend="uia").connect(
                title=_t["Google Earth Pro"])
            break
        except Exception as e:
            print(e)
    main_win = earth_app.window(title=_t["Google Earth Pro"])
    thumb_thread = threading.Thread(target=get_screenshot, args=(main_win, output_dir))
    # thumb_thread.start()

    # poll until movie make window could be found
    while True:
        time.sleep(3)
        try:
            main_win.set_focus()
            main_win[_t["Tools"]].click_input()

            for win in earth_app.windows():
                if win.friendly_class_name() == "Menu":
                    tools_menu = win.items()

            for item in tools_menu:
                if item.texts() == [_t["Movie Maker"]]:
                    item.click_input()

            moviemaker_win = earth_app.window(title=_t["Movie Maker"])
            break
        except Exception as e:
            print(e)

    # select tour (list item 3)
    moviemaker_win[_t["A selected tour:"]].click_input()
    moviemaker_win[_t["Record from Down"]].click_input()
    moviemaker_win[_t["List Item3"]].click_input()

    # enter absolute path
    output_path_noext = pathlib.Path(output_dir).joinpath("earthpro")
    moviemaker_win[_t["Save to"]].click_input()
    send_keys("^a" + str(output_path_noext))

    # select resolution
    resolution_menu = moviemaker_win[_t["Video Parameters"]].child_window(
        title=_t["Video parameters Down"], control_type="ComboBox")
    resolution_menu.click_input()
    resolution_menu.child_window(title=_t["(Custom)"]).click_input()

    # enter width
    moviemaker_win[_t["Video parametersUpDown"]].click_input()
    send_keys("^a" + str(task_info["width"]))

    # enter height
    moviemaker_win[_t["Video parametersUpDown2"]].click_input()
    send_keys("^a" + str(task_info["height"]))

    # enter fps
    moviemaker_win[_t["Video parametersUpDown3"]].click_input()
    send_keys("^a" + str(task_info["framerate"]))

    # select codec
    filetype_menu = moviemaker_win[_t["Output configuration"]].child_window(
        title=_t["Output configuration Down"], control_type="ComboBox", found_index=0)
    filetype_menu.click_input()
    filetype_menu.child_window(
        title=_t["H.264 (.m4v)"], control_type="ListItem").click_input()

    # select quality
    moviemaker_win[_t["Output configuration DownComboBox2"]].click_input()
    moviemaker_win.child_window(
        title=_t["High"], control_type="ListItem").click_input()

    # start rendering
    moviemaker_win[_t["Create Movie Enter"]].click_input()
    thumb_thread.start()

    # wait until finished
    while True:
        time.sleep(3)
        try:
            pygetwindow.getWindowsWithTitle('Movie Maker')[0]
        except:
            time.sleep(3)
            earth_proc.kill()
            break
    
    thumb_thread.join()

    # wrap output video in mp4 format
    subprocess.call([
        FFMPEG_EXECUTABLE, "-v", "warning", "-stats", "-y",
        "-i", str(output_path_noext.with_suffix(".m4v")), 
        "-c:v", "copy",
        str(output_path_noext.with_suffix(".mp4"))])
    os.remove(str(output_path_noext.with_suffix(".m4v")))

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--task-info", "-t")
    parser.add_argument("--missionlog-path", "-m")
    parser.add_argument("--output-dir", "-o")
    args = parser.parse_args()

    render(json.loads(args.task_info), args.missionlog_path, args.output_dir)
