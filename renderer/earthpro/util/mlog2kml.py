import argparse
import simplekml
import numpy as np
import sys
import datetime


class Quaternion:
    UNIT_X = (1, 0, 0)
    UNIT_Y = (0, 1, 0)
    UNIT_Z = (0, 0, 1)

    def __init__(self, x=1, y=0, z=0, w=0):
        self.x = x
        self.y = y
        self.z = z
        self.w = w

    @staticmethod
    def multiply(left, right):
        x = left.w * right.x + left.x * right.w + left.y * right.z - left.z * right.y
        y = left.w * right.y - left.x * right.z + left.y * right.w + left.z * right.x
        z = left.w * right.z + left.x * right.y - left.y * right.x + left.z * right.w
        w = left.w * right.w - left.x * right.x - left.y * right.y - left.z * right.z
        return Quaternion(x, y, z, w)

    @staticmethod
    def from_normalized_axis_angle(axis, angle):
        # assume axis is normalized
        half_angle = angle / 2.0
        s, c = np.sin(half_angle), np.cos(half_angle)
        return Quaternion(axis[0] * s, axis[1] * s, axis[2] * s, c)

    @staticmethod
    def from_hpr(h, p, r):
        srq = Quaternion.from_normalized_axis_angle(Quaternion.UNIT_X, r)
        spq = Quaternion.from_normalized_axis_angle(Quaternion.UNIT_Y, -p)
        result = Quaternion.multiply(spq, srq)
        shq = Quaternion.from_normalized_axis_angle(Quaternion.UNIT_Z, -h)
        return Quaternion.multiply(shq, result)

    @staticmethod
    def to_hpr(q):
        t = 2 * (q.w * q.y - q.z * q.x)
        dr = 1 - 2 * (q.x * q.x + q.y * q.y)
        nr = 2 * (q.w * q.x + q.y * q.z)
        dh = 1 - 2 * (q.y * q.y + q.z * q.z)
        nh = 2 * (q.w * q.z + q.x * q.y)
        h = -np.arctan2(nh, dh)
        r = np.arctan2(nr, dr)
        p = -np.arcsin(max(-1, min(1, t)))
        return h, p, r


def convert_kml(task_info, missionlog_path, kml_path):
    kml = simplekml.Kml(name="MissionLogX")
    kml.newgxtour(name="Tour_X")
    tour = kml.newgxtour(name="Tour_Y")
    playlist = tour.newgxplaylist()
    mlog = np.loadtxt(missionlog_path, skiprows=1)

    camera_offset_quat = Quaternion.from_hpr(
        np.deg2rad(np.float64(task_info["camera_pan"])),
        np.deg2rad(-np.float64(task_info["camera_tilt"])),
        np.deg2rad(np.float64(task_info["camera_roll"]))
    )

    time_offset_iso = datetime.datetime.fromtimestamp(task_info["renderer_params"]["time_offset"] / 1000).isoformat()

    prev_time = 0
    for wp in mlog:
        time, lon, lat, alt, roll, pitch, heading, _, _, _ = wp
        wp_quat = Quaternion.from_hpr(heading, pitch, roll)
        camera_quat = Quaternion.multiply(wp_quat, camera_offset_quat)
        heading, pitch, roll = Quaternion.to_hpr(camera_quat)

        flyto = playlist.newgxflyto(gxduration=(time - prev_time))
        prev_time = time

        pitch = np.pi / 2 - pitch
        # kml tilt does not seem to handle pitch negative angles
        if pitch < 0:
            pitch = -pitch
            heading = -heading

        flyto.camera.longitude = np.rad2deg(lon)
        flyto.camera.latitude = np.rad2deg(lat)
        flyto.camera.altitude = np.round(alt, 2)
        flyto.camera.altitudemode = simplekml.AltitudeMode.absolute
        flyto.camera.heading = np.round(np.rad2deg(heading), 2)
        flyto.camera.tilt = np.round(np.rad2deg(pitch), 2)
        flyto.camera.roll = np.round(np.rad2deg(roll), 2)
        # Added to get fov (camera params) from Automation GUI
        flyto.camera.gxhorizfov = np.float64(task_info["fov"])
        flyto.camera.gxtimestamp.when = time_offset_iso

    kml.save(kml_path, format=False)
