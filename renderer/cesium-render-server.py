#!/usr/bin/env python3

from flask import Flask, request, jsonify, send_file
from flask_cors import CORS
from flask_sockets import Sockets
import subprocess
import sys
import datetime
import time
from TaskDbConnector import TaskDbConnector

app = Flask(__name__, static_url_path="/static", static_folder="static")
CORS(app)
sockets = Sockets(app)

BROWSER_CMD = ["firefox", "-p", "headless", "http://localhost:5001/static/cesium-renderer.html"]
#BROWSER_CMD = ["chromium-browser", "http://localhost:5000/static/cesium-renderer.html"]
#BROWSER_CMD = ["/opt/google/chrome/chrome", "http://localhost:5000/static/cesium-renderer.html"]

ffmpeg_proc = None
taskdb_conn = None
current_task = None
browser_proc = None

@app.route("/start", methods=["POST"])
def start():
    global ffmpeg_proc, taskdb_conn, current_task, browser_proc

    task_id = request.json["task_id"]
    current_task = taskdb_conn.get_task(task_id)

    ffmpeg_cmd = ["ffmpeg", "-y", "-f", "image2pipe"]
    ffmpeg_cmd += ["-framerate", str(current_task["framerate"])]
    ffmpeg_cmd += ["-i", "-"]
    ffmpeg_cmd += current_task["encoding_params"]
    ffmpeg_cmd += [current_task["output_path"]]

    ffmpeg_proc = subprocess.Popen(ffmpeg_cmd, stdin=subprocess.PIPE)
    browser_proc = subprocess.Popen(BROWSER_CMD)
    return jsonify(success=True)

@app.route("/mission-log", methods=["GET"])
def get_mission_log():
    global current_task
    return send_file(current_task["missionlog_path"],
        mimetype="text/plain",
        as_attachment=True, cache_timeout=0)

@app.route("/task-info", methods=["GET"])
def get_task_info():
    global current_task
    return current_task

@app.route("/encode-end", methods=["POST"])
def stop():
    global ffmpeg_proc, browser_proc
    
    ffmpeg_proc.stdin.close()
    ffmpeg_proc.wait()
    ffmpeg_proc = None

    browser_proc.wait()
    
    return jsonify(success=True)

@sockets.route("/encode")
def encode_socket(ws):
    while not ws.closed:
        global ffmpeg_proc
        message = ws.receive()
        if message is not None:
            ffmpeg_proc.stdin.write(message)
            ws.send("next")

if __name__ == '__main__':
    from gevent import pywsgi
    from geventwebsocket.handler import WebSocketHandler

    taskdb_conn = TaskDbConnector()
    taskdb_conn.connect()

    server = pywsgi.WSGIServer(("", 5001), app, handler_class=WebSocketHandler)
    server.serve_forever()
