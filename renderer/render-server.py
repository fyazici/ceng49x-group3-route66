from flask import Flask, request, jsonify
from flask_cors import CORS
import subprocess
import sys
import time
import requests
import tempfile
import os
import pathlib
import json
import threading
import concurrent.futures

app = Flask(__name__, static_folder="static", static_url_path="/static")
CORS(app)

try:
    MAX_CONCURRENT_RENDERS = int(os.environ["R66_MAX_CONCURRENT_RENDERS"])
except KeyError:
    MAX_CONCURRENT_RENDERS = 1
BACKEND_URL = "http://127.0.0.1:5000"


class PortPool:
    def __init__(self, available_ports):
        self.free_ports = list(available_ports)
        self.used_ports = []
        self._lk = threading.Lock()

    def take_port(self):
        self._lk.acquire()
        port = self.free_ports.pop()
        self.used_ports.append(port)
        self._lk.release()
        return port

    def give_port(self, port):
        self._lk.acquire()
        self.used_ports.remove(port)
        self.free_ports.append(port)
        self._lk.release()


port_pool = PortPool(range(5002, 5002 + MAX_CONCURRENT_RENDERS))
render_pool_sem = threading.Semaphore(MAX_CONCURRENT_RENDERS)
render_next_lock = threading.Lock()
# max_workers can be any number >= MAX_CONCURRENT_RENDERS
render_pool_exec = concurrent.futures.ThreadPoolExecutor(max_workers=MAX_CONCURRENT_RENDERS)
# NOTE: Unfortunately Google Earth Pro cannot open multiple instances
google_earth_instance_lock = threading.Lock()
google_earth_instance_task_id = 0


def get_missionlog(task_info):
    # TODO: put filename in request body
    task_id = task_info["task_id"]
    name = pathlib.Path(task_info["missionlog_path"]).name
    resp = requests.get(BACKEND_URL + "/log-file?name={}".format(name), stream=True)
    path = pathlib.Path("static/task/{}/{}".format(task_id, name))
    with open(path, "wb") as f:
        for chunk in resp:
            f.write(chunk)
    return path


def get_output_dir(task_info):
    return pathlib.Path("static/task/{}".format(task_info["task_id"]))


def render_cesium(task_info):
    exec_port = port_pool.take_port()
    exec_env = os.environ.copy()
    exec_env["R66_RENDER_PORT"] = str(exec_port)

    # get mission log
    missionlog_path = get_missionlog(task_info)
    output_dir = get_output_dir(task_info)

    # run and wait
    subprocess.call([
        sys.executable, "main.py",
        "-t", json.dumps(task_info),
        "-m", str(missionlog_path.resolve()),
        "-o", str(output_dir.resolve())],
        env=exec_env, cwd="cesium")

    # release port allocation
    port_pool.give_port(exec_port)


def render_earthpro(task_info):
    global google_earth_instance_task_id
    if google_earth_instance_lock.locked():
        print("Warning: Earth Pro renderer is limited to single instance.")
        print("Task {} will wait for task {}".format(task_info["task_id"], google_earth_instance_task_id))
    # block if another instance is running
    google_earth_instance_lock.acquire()
    google_earth_instance_task_id = task_info["task_id"]

    # get mission log
    missionlog_path = get_missionlog(task_info)
    output_dir = get_output_dir(task_info)
    
    # run and wait
    subprocess.call([
        sys.executable, "main.py",
        "-t", json.dumps(task_info),
        "-m", str(missionlog_path.resolve()),
        "-o", str(output_dir.resolve())],
        cwd="earthpro")
    
    # release instance lock
    google_earth_instance_lock.release()


def render_depth(task_info):
    # get camconf
    camconf_task_id = task_info["renderer_params"]["camconf_task_id"]
    camconf_path = pathlib.Path("static/task/{}/camconf.jsons".format(camconf_task_id))
    output_dir = get_output_dir(task_info)

    # run and wait
    subprocess.call([
        sys.executable, "main.py",
        "-t", json.dumps(task_info),
        "-c", str(camconf_path.resolve()),
        "-o", str(output_dir.resolve())],
        cwd="depth")


def render_fogcloud(task_info):
    depth_task_id = task_info["renderer_params"]["depth_task_id"]
    original_task_id = task_info["renderer_params"]["original_task_id"]
    depth_path = pathlib.Path("static/task/{}/depth.mp4".format(depth_task_id))
    valid_path = pathlib.Path("static/task/{}/valid.mp4".format(depth_task_id))
    # try cesium by default
    original_path = pathlib.Path("static/task/{}/cesium.mp4".format(original_task_id))
    if not original_path.exists():
        # must be earthpro
        original_path = pathlib.Path("static/task/{}/earthpro.mp4".format(original_task_id))
    output_dir = get_output_dir(task_info)
    
    subprocess.call([
        sys.executable, "fog_process.py",
        "-t", json.dumps(task_info),
        "-s", str(original_path.resolve()),
        "-d", str(depth_path.resolve()),
        "-v", str(valid_path.resolve()),
        "-o", str(output_dir.resolve())],
        cwd="effects")


def render_exec(task_info):
    task_id = task_info["task_id"]
    renderer_type = task_info["renderer_params"]["renderer"]
    print("Executing task id {}".format(task_id))

    # create task directory
    pathlib.Path("static/task/{}".format(task_id)).mkdir(exist_ok=True)

    if renderer_type == "cesiumjs":
        render_cesium(task_info)
    elif renderer_type == "earthpro":
        render_earthpro(task_info)
    elif renderer_type == "depth":
        render_depth(task_info)
    elif renderer_type == "fogcloud":
        render_fogcloud(task_info)
    else:
        print("Unknown task type: ", renderer_type)

    # inform backend that the task is finished
    requests.post(BACKEND_URL + "/api/v1/task-finished", json={
        "task_id": task_info["task_id"]
    })

    # ping pong action between exec and next_task
    render_pool_sem.release()
    render_pool_exec.submit(render_next_task)


def render_next_task():
    print("Start")
    # do not run if another next task request is running
    if render_next_lock.acquire(blocking=False):
        # NOTE: loop breaks if:
        # 1) no more tasks left, or
        # 2) max concurrent renderers reached
        # So it is safe to call this fn arbitrarily many times
        while render_pool_sem.acquire(blocking=False):
            # NOTE: is this related to the error below?
            # sqlite3.ProgrammingError: SQLite objects created in a thread can only be used in that same thread
            time.sleep(1)
            # TODO: GET is a better fit here?
            resp = requests.post(BACKEND_URL + "/api/v1/new-task").json()
            if resp["message"] == "sleep":
                # no need to do anything
                print("Sleep")
                render_pool_sem.release()
                break
            else:
                task_info = resp["task"]
                render_pool_exec.submit(render_exec, task_info)
        render_next_lock.release()


@app.route("/wake-up", methods=["POST"])
def wake_up():
    print("Wake up")
    render_pool_exec.submit(render_next_task)
    return jsonify(success=True)

@app.route("/health-check")
def health_check():
    return jsonify(success=True)

if __name__ == '__main__':
    app.run(port=5001)
