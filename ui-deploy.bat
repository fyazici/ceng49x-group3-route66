@echo off

rmdir /s /q ui\templates

pushd frontend

call npm i
call npm run build
robocopy /s /e build ..\ui\templates

popd