import { createBrowserHistory } from 'history';

// We are using history to navigate. The description below is quoted from https://www.npmjs.com/package/history

// The history library lets you easily manage session history anywhere JavaScript runs. 
// history abstracts away the differences in various environments and provides a minimal API that 
// lets you manage the history stack, navigate, and persist state between sessions.

export const history = createBrowserHistory();