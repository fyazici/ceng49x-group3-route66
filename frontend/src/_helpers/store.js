import { createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
//import { createLogger } from 'redux-logger';
import rootReducer from '../_reducers';

//const loggerMiddleware = createLogger();

export const store = createStore(
    rootReducer,
    applyMiddleware(
        thunkMiddleware, // One of many version of redux like Thunk, Saga. I choose it because I am familiar with it.
//      loggerMiddleware // A tool to monitor app in case of something went wrong.
    )
);