import React, { Component } from 'react';
import { Router, Route, Switch } from 'react-router-dom';
import { connect } from 'react-redux';

import { history } from './_helpers';
import { PrivateRoute } from './_components';
import { LoginPage } from './_pages/LoginPage';
import { VideoPage } from './_pages/VideoPage'
import { Header } from './_components/header';
import { TaskPage } from './_pages/TaskPage';
import { LogPage } from './_pages/LogPage/LogPage';
import { ErrorPage } from './_pages/ErrorPage';
import { SignUpPage } from './_pages/SignUpPage';

import './App.css';

class App extends Component
{
	render()
	{
		return (
			<div>
				<Header />
				<div className="jumbotron">
					<div className="container">
						<Router history={history}>
							<div className="content">
								<Switch>
									<PrivateRoute exact path="/" component={TaskPage} />
									<PrivateRoute exact path="/video" component={VideoPage} />
									<PrivateRoute exact path="/tasks" component={TaskPage} />
									<PrivateRoute exact path="/logs" component={LogPage} />
									<Route exact path="/login" component={LoginPage} />
									<Route exact path="/register" component={SignUpPage} />
									<Route component={ErrorPage} />
								</Switch>
							</div>
						</Router>
					</div>
				</div>
			</div>


		);
	}
}

const connectedApp = connect()(App);
export { connectedApp as App }; 