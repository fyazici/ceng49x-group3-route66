import { taskConstants } from '../_constants';
import { history } from '../_helpers';

export const taskActions = {
    selectAndWatchSolo,
    selectAndWatchDuo,
};

function selectAndWatchSolo(videoName, taskFirst)
{
    const data = {
        videoNameFirst: videoName, 
        videoNameSecond: null,
        taskFirst: taskFirst
    };

    sessionStorage.setItem('videoNameFirst', videoName);
    sessionStorage.setItem('isSolo', true);
    sessionStorage.setItem('taskFirst', JSON.stringify(taskFirst));

    return dispatch => {
        dispatch({ type: taskConstants.TASK_SELECTED_SOLO, data });
        setTimeout(history.push("/video"), 100);
    };
}

function selectAndWatchDuo(videoNameFirst, videoNameSecond, taskFirst, taskSecond)
{
    const data = {
        videoNameFirst: videoNameFirst, 
        videoNameSecond: videoNameSecond,
        taskFirst: taskFirst,
        taskSecond: taskSecond
    };

    sessionStorage.setItem('videoNameFirst', videoNameFirst);
    sessionStorage.setItem('videoNameSecond', videoNameSecond);
    sessionStorage.setItem('isSolo', false);
    sessionStorage.setItem('taskFirst', JSON.stringify(taskFirst));
    sessionStorage.setItem('taskSecond', JSON.stringify(taskSecond));

    return dispatch => {
        dispatch({ type: taskConstants.TASK_SELECTED_DUO, data });
        setTimeout(history.push("/video"), 100);
    };
}
