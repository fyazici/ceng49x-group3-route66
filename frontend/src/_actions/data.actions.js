import axios from 'axios';
import { apiConstants, dataConstants } from '../_constants';

export const dataActions = {
    getUserLogs,
    getUserTasks,
    sendLogFile
};

async function sendLogFile(logFile, setProgress) {
    const formData = new FormData();

    const user = sessionStorage.getItem('user');

    var config = {
        onUploadProgress: function(progressEvent) {
          var percentCompleted = Math.round( (progressEvent.loaded * 100) / progressEvent.total );
          setProgress(percentCompleted);
        }
      };

    formData.append('file', logFile);
    formData.append('token', user);
    formData.append('log_file_name', logFile.name);

    return axios.post(
        apiConstants.BASE_URL + '/upload-log-file',
        formData,
        config
        );
}

function getUserLogs()
{
    return async dispatch =>
    {
        const user = sessionStorage.getItem('user');
        if (user)
        {
            dispatch(requested());

            axios({
                method: 'post',
                url: apiConstants.BASE_URL + '/my-files',
                data: {
                    token: user,
                },
            })
                .then(response =>
                {
                    if (response.status === 200)
                    {
                        if(response.data.message === "success"){
                            dispatch(success(response.data.files || []))
                            sessionStorage.setItem('logs',JSON.stringify(response.data.files || []));
                        }
                        else{
                            dispatch(failure("Can not retrieved.",response.data.message));
                        }
                    }
                    else
                    {
                        dispatch(failure("Can not retrieved."));
                    }

                })
                .catch(error =>
                {
                    dispatch(failure(error.message));
                });
        }

        else
        {
            dispatch(failure("THERE SHOULD BE USER. INVESTIGATE!"));
        }


        function requested() { return { type: dataConstants.LOG_DATA_REQUESTED } }
        function success(data) { return { type: dataConstants.LOG_DATA_SUCCESS, data } }
        function failure(error) { return { type: dataConstants.LOG_DATA_FAILED, error } }

    }
};


function getUserTasks()
{
    axios.post(apiConstants.RENDER_SERVER_URL + '/wake-up');
    return async dispatch =>
    {
        const user = sessionStorage.getItem('user');
        if (user)
        {
            dispatch(requested());

            axios({
                method: 'post',
                url: apiConstants.BASE_URL + '/my-tasks',
                data: {
                    token: user,
                }
            })
                .then(response =>
                {
                    if (response.status === 200)
                    {   
                        if(response.data.message === "success"){
                            dispatch(success(response.data.tasks || []))
                            sessionStorage.setItem('tasks',JSON.stringify(response.data.tasks || []));
                        }
                        else{
                            dispatch(failure("Can not retrieved.",response.data.message));
                        }
                    }
                    else
                    {
                        dispatch(failure("Can not retrieved."));
                    }

                })
                .catch(error =>
                {
                    dispatch(failure(error.message));
                });
        }

        else
        {
            dispatch(failure("THERE SHOULD BE USER. INVESTIGATE!"));
        }


        function requested() { return { type: dataConstants.TASK_DATA_REQUESTED } }
        function success(data) { return { type: dataConstants.TASK_DATA_SUCCESS, data } }
        function failure(error) { return { type: dataConstants.TASK_DATA_FAILED, error } }

    }
};