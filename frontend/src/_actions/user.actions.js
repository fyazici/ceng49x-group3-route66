import axios from 'axios';
import { userConstants, apiConstants } from '../_constants';
import { alertActions } from './';
import { history } from '../_helpers';

export const userActions = {
    login,
    logout,
    signUp,
};

let refleshJWT_Interval = null;

function refleshJWT(username, password, dispatch1){
    refleshJWT_Interval = setInterval( () =>{
            axios({
            method: 'post',
            url: apiConstants.BASE_URL + '/login',
            data: {
                user_name: username,
                password: password
            }
            })
            .then(response =>
            {
                if (response.status === 200 && response.data.token)
                {
                    sessionStorage.setItem('user', response.data.token);
                    dispatch1(success(response.data.token));
                }
                else{
                    history.push('/login');
                }
    
            })
            .catch(error =>
            {
                history.push('/login');
            });
    },60e3)
    function success(user) { return { type: userConstants.LOGIN_SUCCESS, user } }
}

function login(username, password)
{
    cleanLocalStore();

    return async dispatch =>
    {
        axios({
            method: 'post',
            url: apiConstants.BASE_URL + '/login',
            data: {
                user_name: username,
                password: password
            }
        })
            .then(response =>
            {
                if (response.status === 200)
                {
                    if(response.data.token){
                        sessionStorage.setItem('user', response.data.token);
                        dispatch(success(response.data.token));
                        history.push('/tasks');
                        refleshJWT(username, password, dispatch);
                        return;
                    }
                }
                dispatch(failure("User login failed."));
                dispatch(alertActions.error("User login failed."));

            })
            .catch(error =>
            {
                dispatch(failure(error.message));
                dispatch(alertActions.error(error.message));
            });

    };

    function success(user) { return { type: userConstants.LOGIN_SUCCESS, user } }
    function failure(error) { return { type: userConstants.LOGIN_FAILURE, error } }
}

function logout()
{
    return async dispatch =>
    {
        axios({
            method: 'post',
            url: apiConstants.BASE_URL + '/logout',
            data: {
                token: sessionStorage.getItem('user')
            }
        })
            .then(() =>
            {
                dispatch({ type: userConstants.LOGOUT });
                cleanLocalStore();
            })
            .catch(e =>
            {
                console.log("Backend error:", e)
                cleanLocalStore();
            });
        dispatch({ type: userConstants.LOGOUT });
        cleanLocalStore();
    }

}


function signUp(username, password, name, surname)
{
    cleanLocalStore();
    return async dispatch =>
    {
        axios({
            method: 'post',
            url: apiConstants.BASE_URL + '/sign-up',  // TO DO SHOULD BE SIGN - UP BACKEND- FIX REQUIRED
            data: {
                user_name: username,
                password: password,
                name: name,
                surname: surname
            }
        })
            .then(response =>
            {
                if (response.data.status === 801)
                {
                    dispatch(alertActions.error("Username already taken."));
                }
                else if (response.status === 200)
                {
                    dispatch(login(username, password))
                }
                else
                {
                    dispatch(alertActions.error("Sign-up failed."));
                }

            })
            .catch(error =>
            {
                dispatch(alertActions.error(error.message));
            });
    };
}

function cleanLocalStore(){
    clearInterval(refleshJWT_Interval)
    sessionStorage.removeItem('user');
    sessionStorage.removeItem('tasks');
    sessionStorage.removeItem('logs');
    sessionStorage.removeItem('isSolo');
    sessionStorage.removeItem('videoNameFirst');
    sessionStorage.removeItem('videoNameSecond');
    sessionStorage.removeItem('taskFirst');
    sessionStorage.removeItem('taskSecond');
}