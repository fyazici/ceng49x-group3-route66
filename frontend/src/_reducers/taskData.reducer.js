import { dataConstants } from '../_constants';

const { TASK_DATA_REQUESTED, TASK_DATA_SUCCESS, TASK_DATA_FAILED } = dataConstants;

const INITIAL_STATE = {
    isRetrieved: false,
    taskList: [],
    loading: false
}
export function taskData(state = {}, action)
{
    switch (action.type)
    {
        case TASK_DATA_REQUESTED:
            return { ...state, loading: true }

        case TASK_DATA_SUCCESS:
            return { isRetrieved: true, taskList: action.data, loading: false }

        case TASK_DATA_FAILED:
            return INITIAL_STATE;

        default:
            return state
    }
}