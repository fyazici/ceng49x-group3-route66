import { userConstants } from '../_constants';

const { LOGIN_FAILURE, LOGIN_SUCCESS, LOGOUT } = userConstants;

const user = sessionStorage.getItem('user');

const INITIAL_STATE = user ?
	{
		loggedIn: true,
		user: user,
	} :
	{
		loggedIn: false,
		user: null,
	};


export function authentication(state = INITIAL_STATE, action)
{
	switch (action.type)
	{
		case LOGIN_SUCCESS:
			return {
				loggedIn: true,
				user: action.user,
			};
		case LOGIN_FAILURE:
			return INITIAL_STATE;

		case LOGOUT:
			return {
				loggedIn: false,
				user: null
			};

		default:
			return state
	}
}