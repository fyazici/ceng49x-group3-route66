import { taskConstants } from '../_constants';

const { TASK_SELECTED_SOLO, TASK_SELECTED_DUO } = taskConstants;

const INITIAL_STATE = {
    isSolo : null,
    selectedTask : {
        videoNameFirst: null, 
        videoNameSecond: null,
        taskFirst: null,
        taskSecond: null
    }
}

export function aboutTasks(state = INITIAL_STATE, action)
{
    switch (action.type)
    {
        case TASK_SELECTED_SOLO:
            return { isSolo: true, selectedTask: action.data }

        case TASK_SELECTED_DUO:
            return { isSolo: false, selectedTask: action.data }

        default:
            return state
    }
}