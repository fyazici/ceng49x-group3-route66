import { combineReducers } from 'redux';

import { authentication } from './authentication.reducer';
import { alert } from './alert.reducer';
import { taskData } from './taskData.reducer';
import { logData } from './logData.reducer';
import { aboutTasks } from './task.reducer';

const rootReducer = combineReducers({
  authentication,
  alert,
  taskData,
  logData,
  aboutTasks
});

export default rootReducer;