import { dataConstants } from '../_constants';

const { LOG_DATA_REQUESTED, LOG_DATA_SUCCESS, LOG_DATA_FAILED } = dataConstants;

const INITIAL_STATE = {
    isRetrieved: false,
    logList: [],
    loading: false
}

export function logData(state = {}, action)
{
    switch (action.type)
    {
        case LOG_DATA_REQUESTED:
            return { ...state, loading: true }

        case LOG_DATA_SUCCESS:
            return { isRetrieved: true, logList: action.data, loading: false }

        case LOG_DATA_FAILED:
            return INITIAL_STATE;

        default:
            return state
    }
}