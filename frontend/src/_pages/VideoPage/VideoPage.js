import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import ReactPlayer from 'react-player';
import screenfull from 'screenfull'

import { apiConstants } from '../../_constants'

import './VideoPage.css';

class VideoPage extends Component
{
    constructor(props)
    {
        super(props);
        this.state = {
            videoNameFirst: sessionStorage.getItem('videoNameFirst'),
            videoNameSecond: sessionStorage.getItem('videoNameSecond'),
            isSolo : sessionStorage.getItem('isSolo') === "true",
            url: apiConstants.RENDER_SERVER_URL + "/static/task/",
            playing: false,
            controls: false,
            light: false,
            volume: 0.0,
            muted: true,
            played: 0,
            loaded: 0,
            playbackRate: 1.0,
            loop: false,
            playedSeconds: 0.0,
        }
    }

    load = url =>
    {
        this.setState({
            url,
            played: 0,
            loaded: 0,
        })
    }

    handlePlayPause = () =>
    {
        this.setState({ playing: !this.state.playing })
    }

    handlePlay = () =>
    {
        //console.log('onPlay')
        this.setState({ playing: true })
    }

    handlePause = () =>
    {
        //console.log('onPause')
        this.setState({ playing: false })
    }

    handleStop = () =>
    {
        //console.log('onStop')
        this.setState({
            playing: false,
            played: 0,
            playbackRate: 1.0
        })
        this.player.seekTo(parseFloat(0))
        this.player2.seekTo(parseFloat(0))
    }

    handleSetPlaybackRate = e =>
    {
        this.setState({ playbackRate: parseFloat(e.target.value) })
    }

    handleRewind = () =>
    {
        //console.log("Rewind")
        const playedSeconds = (this.state.playedSeconds - 5) < 0 ? 0 : (this.state.playedSeconds - 5);
        this.player.seekTo(playedSeconds, 'seconds')
        this.player2.seekTo(playedSeconds, 'seconds')
        this.setState({ playedSeconds })
    }

    handleFastForward = () =>
    {
        //console.log("FastForward")
        this.player.seekTo(this.state.playedSeconds + 5, 'seconds')
        this.player2.seekTo(this.state.playedSeconds + 5, 'seconds')
        this.setState({ playedSeconds: (this.state.playedSeconds + 5) })
    }

    handleClickFullscreenLeft = () =>
    {
        screenfull.request(ReactDOM.findDOMNode(this.player))
    }

    handleClickFullscreenRight = () =>
    {
        screenfull.request(ReactDOM.findDOMNode(this.player2))
    }

    handleSeekMouseDown = e =>
    {
        this.setState({ seeking: true })
    }

    handleSeekChange = e =>
    {
        this.setState({ played: parseFloat(e.target.value) })
    }

    handleSeekMouseUp = e =>
    {
        this.setState({ seeking: false })
        this.player.seekTo(parseFloat(e.target.value))
        this.player2.seekTo(parseFloat(e.target.value))
    }

    handleProgress = state =>
    {
        //console.log('onProgress', state)
        if (!this.state.seeking)
        {
            this.setState(state)
        }
    }

    handleEnded = () =>
    {
        //console.log('onEnded')
        this.setState({ playing: this.state.loop })
    }

    ref = player =>
    {
        this.player = player
    }

    ref2 = player =>
    {
        this.player2 = player
    }

    render()
    {
        const { url, playing, videoNameFirst, videoNameSecond, controls, light, volume, muted, loop, played, loaded, playbackRate } = this.state
        const { aboutTasks } = this.props;
        return (
        <div>
            { aboutTasks.isSolo || this.state.isSolo
                ?
                <div className='videoPlayer'>
                    <h2 title={sessionStorage.getItem('taskFirst')}>{(aboutTasks.taskFirst && aboutTasks.taskFirst.name) || JSON.parse(sessionStorage.getItem('taskFirst')).name}</h2>
                    <div className='player-wrapper'>
                        <ReactPlayer
                            ref={this.ref}
                            className='react-player'
                            width='100%'
                            height='100%'
                            url={url + (aboutTasks.selectedTask.videoNameFirst || videoNameFirst)}
                            playing={playing}
                            controls={true}
                            light={light}
                            loop={loop}
                            volume={volume}
                            muted={muted}
                        />
                    </div>
                </div>
                :
                <div className='videoPlayer'>
                    <h2 title={sessionStorage.getItem('taskFirst')}>{(aboutTasks.taskFirst && aboutTasks.taskFirst.name) || JSON.parse(sessionStorage.getItem('taskFirst')).name}</h2>

                    <div className='players' onContextMenu={(e)=> e.preventDefault()}>
                        <div className='player-wrapper'>
                            <ReactPlayer
                                ref={this.ref}
                                className='react-player'
                                width='100%'
                                height='100%'
                                url={url + (aboutTasks.selectedTask.videoNameFirst || videoNameFirst)}
                                playing={playing}
                                controls={controls}
                                light={light}
                                loop={loop}
                                playbackRate={playbackRate}
                                volume={volume}
                                muted={muted}
                                //onReady={() => console.log('onReady')}
                                //onStart={() => console.log('onStart')}
                                onPlay={this.handlePlay}
                                onPause={this.handlePause}
                                //onBuffer={() => console.log('onBuffer')}
                                //onSeek={e => console.log('onSeek', e)}
                                onEnded={this.handleEnded}
                                onError={e => console.log('onError', e)}
                                onProgress={this.handleProgress}
                                onDuration={this.handleDuration}
                            />
                        </div>
                        <div className='player-wrapper'>
                            <ReactPlayer
                                ref={this.ref2}
                                className='react-player'
                                width='100%'
                                height='100%'
                                url={url + (aboutTasks.selectedTask.videoNameSecond || videoNameSecond)}
                                playing={playing}
                                controls={controls}
                                light={light}
                                loop={loop}
                                playbackRate={playbackRate}
                                volume={volume}
                                muted={muted}
                                //onReady={() => console.log('onReady')}
                                //onStart={() => console.log('onStart')}
                                onPlay={this.handlePlay}
                                onPause={this.handlePause}
                                //onBuffer={() => console.log('onBuffer')}
                                //onSeek={e => console.log('onSeek', e)}
                                onEnded={this.handleEnded}
                                onError={e => console.log('onError', e)}
                                onProgress={this.handleProgress}
                                onDuration={this.handleDuration}
                            />
                        </div>
                    </div>
                    <div className="infoTable">
                        <table>
                            <tbody>
                                <tr>
                                    <th>Controls</th>
                                    <td>
                                        <button className="btn btn-secondary btn-sm" onClick={this.handleRewind}>&lt;&lt;</button>
                                        <button className="btn btn-secondary btn-sm"onClick={this.handlePlayPause}>{playing ? 'Pause' : 'Play'}</button>
                                        <button className="btn btn-secondary btn-sm"onClick={this.handleStop}>Stop</button>
                                        <button className="btn btn-secondary btn-sm"onClick={this.handleFastForward}>&gt;&gt;</button>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Playback Rate</th>
                                    <th>
                                        <button className="btn btn-secondary btn-sm"onClick={this.handleSetPlaybackRate} value={0.5}>0.5x</button>
                                        <button className="btn btn-secondary btn-sm"onClick={this.handleSetPlaybackRate} value={2}>2x</button>
                                        <button className="btn btn-secondary btn-sm"onClick={this.handleSetPlaybackRate} value={4}>4x</button>
                                    </th>
                                </tr>
                                <tr>
                                    <th>FullScreen</th>
                                    <th>
                                        <button className="btn btn-secondary btn-sm" onClick={this.handleClickFullscreenLeft}>LeftVideo</button>
                                        <button className="btn btn-secondary btn-sm" onClick={this.handleClickFullscreenRight}>RightVideo</button>
                                    </th>
                                </tr>
                                <tr>
                                    <th>Download</th>
                                    <th>
                                        <button className="btn btn-secondary btn-sm"
                                            onClick={()=>{
                                                fetch(url+videoNameFirst)
                                                    .then(response => {
                                                        response.blob().then(blob => {
                                                            let url = window.URL.createObjectURL(blob);
                                                            let a = document.createElement('a');
                                                            a.href = url;
                                                            a.download = videoNameFirst;
                                                            a.click();
                                                        });
                                                });
                                            }}>
                                            Flight Video
                                        </button>
                                 
                                        <button className="btn btn-secondary btn-sm"
                                            onClick={()=>{
                                                fetch(url+videoNameSecond)
                                                    .then(response => {
                                                        response.blob().then(blob => {
                                                            let url = window.URL.createObjectURL(blob);
                                                            let a = document.createElement('a');
                                                            a.href = url;
                                                            a.download = videoNameSecond;
                                                            a.click();
                                                        });
                                                });
                                            }}>
                                            {(aboutTasks.taskFirst && aboutTasks.taskFirst.renderer_params.renderer === "depth") 
                                            || (JSON.parse(sessionStorage.getItem('taskFirst')).renderer_params.renderer === "depth")
                                            ?
                                            "Depth Video"
                                            :
                                            "Fog Video"
                                            }
                                        </button>
                                    </th>
                                </tr>
                                <tr>
                                    <th>Seek</th>
                                    <td>
                                        <input
                                            type='range' min={0} max={1} step='any'
                                            value={played}
                                            onMouseDown={this.handleSeekMouseDown}
                                            onChange={this.handleSeekChange}
                                            onMouseUp={this.handleSeekMouseUp}
                                        />
                                    </td>
                                </tr>
                                <tr>
                                    <th>Played</th>
                                    <td><progress max={1} value={played} /></td>
                                </tr>
                                <tr>
                                    <th>Loaded</th>
                                    <td><progress max={1} value={loaded} /></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            }
        </div>
        )
    }
}

function mapStateToProps(state)
{
    const { aboutTasks } = state;
    return {
        aboutTasks
    };
}

const connectedVideoPage = connect(mapStateToProps)(VideoPage);
export { connectedVideoPage as VideoPage };