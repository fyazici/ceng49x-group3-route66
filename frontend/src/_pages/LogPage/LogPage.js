import React, {Component} from 'react';
import {connect} from 'react-redux';

import { dataActions } from '../../_actions';

import "./LogPage.css";

class LogPage extends Component
{

    constructor(props)
    {
        super(props);

        this.state = {
            isError : false,
            isFileSelected : false,
            logFile : null,
            isAddLogMode : false,
            infoMessage: "",
            isShowMessage: false,
            isUploading: false,
            fileTransferPercentage: 0
        };

        this.changeTaskMode = this.changeTaskMode.bind(this);
        this.onFormSubmit = this.onFormSubmit.bind(this)
        this.onFileChange = this.onFileChange.bind(this)
        this.fileUpload = this.fileUpload.bind(this)
        this.onPercentageChange = this.onPercentageChange.bind(this)
    }

    componentDidMount()
    {
        this.props.dispatch(dataActions.getUserLogs());
    }

    changeTaskMode()
    {
        this.setState({
            isAddLogMode: !this.state.isAddLogMode,
            isFileSelected: false,
            logFile: null, 
            infoMessage: "",
            isShowMessage: false,
            isUploading: false,
            percentCompleted: 0,
            isError: false
        });
    }

    onFormSubmit(e){
        e.preventDefault()
        this.setState({isUploading : true})
        this.fileUpload();
    }

    onFileChange(e) {
        this.setState({logFile:e.target.files[0], isFileSelected: true})
    }
    
    onPercentageChange(percentCompleted) {
        this.setState({fileTransferPercentage: percentCompleted})
    }

    async fileUpload(){
        await dataActions.sendLogFile(this.state.logFile, this.onPercentageChange )
        .then(response =>
            {
            if (response.status === 200 && response.data.message === "success")
            {
                this.setState({
                    isFileSelected: false,
                    logFile: null, 
                    isAddLogMode:false, 
                    infoMessage:"File Uploaded Successfully!",
                    isShowMessage: true,
                    isUploading : false,
                    percentCompleted: 0,
                    isError: false
                });
                this.props.dispatch(dataActions.getUserLogs());
            }
            else{
                this.setState({
                    infoMessage:"File could not uploaded! Plase try again.",
                    isShowMessage: true,
                    isUploading : false,
                    percentCompleted: 0,
                    isError: true
                });
            }
        })
        .catch(error =>
        {
            this.setState({
                infoMessage:"File could not uploaded! Plase try again.",
                isShowMessage: true,
                isUploading:false,
                percentCompleted: 0,
                isError:true
        });
        });
        
    }

    logList()
    {
        if(this.props.logData.loading){
            return <tr><td colSpan={2}>Loading</td></tr>;
        }
        if(this.props.logData.isRetrieved){
            if(this.props.logData.logList.length < 1){
                return <tr> <td colSpan={2}>No Log Found</td> </tr>;
            }
            else{
                return (this.props.logData.logList.map( (log, index) => (
                    <tr key={index}>
                        <th scope="row">{log.log_file_id}</th>
                        <td>{log.log_file_name}</td>
                    </tr>
                )));
            }
        }
    }

    render()
    {
        const { isFileSelected, isAddLogMode, isUploading, isShowMessage, infoMessage, isError, fileTransferPercentage } = this.state;

        return (
            <div className="logPage">
                <div className="logViewChange">
                    <button onClick={this.changeTaskMode} hidden={isAddLogMode}>Add Log</button>
                    <p className={"infoMessage " + (isError && "error") } hidden={!isShowMessage}>{infoMessage}</p>
                    {isAddLogMode && 
                        <div className="inputFileDiv"> 
                            <form onSubmit={this.onFormSubmit}>
                                <input type="file" onChange={this.onFileChange} />
                                <button disabled={!isFileSelected} type="submit">Upload</button>
                            </form>
                        </div> 
                    }
                    <div className="progress" hidden={!isUploading}>
                        <div className="progress-bar progress-bar-striped progress-bar-animated bg-success" 
                            role="progressbar" 
                            aria-valuemin="0" 
                            aria-valuemax="100" style={{ "width" : fileTransferPercentage+"%" }}></div>
                    </div>
                </div>
                <div>
                    <h2 style={{ textAlign: 'center', marginBottom: '15px' }}> My Logs </h2>
                    <table className="table">
                        <thead className="thead-dark">
                            <tr>
                                <th scope="col">Log ID</th>
                                <th scope="col">Log File Name</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.logList()}
                        </tbody>
                    </table>
                <div />
            </div>
            </div>
        )
    }
}

function mapStateToProps(state)
{
    const { logData } = state;
    return {
        logData
    };
}

const connectedLogPage = connect(mapStateToProps)(LogPage);
export { connectedLogPage as LogPage };