import React, { Component } from 'react'

import './ErrorPage.css';

class ErrorPage extends Component
{
    render()
    {
        return (
            <div className="banner">
                <h1>404</h1>
                <div />
                <p>Page Not Found</p>
            </div>
        )
    }
}


export { ErrorPage };