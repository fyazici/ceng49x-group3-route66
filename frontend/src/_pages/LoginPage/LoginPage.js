import React, { Component } from 'react';
import { connect } from 'react-redux';
import { history } from '../../_helpers';
import { alertActions, userActions } from '../../_actions';

class LoginPage extends Component
{
    constructor(props)
    {
        super(props);
        const { dispatch } = this.props;

        history.listen((location, action) =>
        {
            // Clear alert on location change. Meaning, if we submit wrong user name or password and reflesh the page
            // or went another url and come back after there should be no error message.
            dispatch(alertActions.clear());
        });

        this.state = {
            username: '',
            password: '',
            submitted: false
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount(){
        window.addEventListener("keyup", event => {
            event.preventDefault();
            if(event.keyCode === 13){
                this.setState({ submitted: true });
                const { username, password } = this.state;
                if (username && password)
                {
                    this.props.dispatch(userActions.login(username, password));
                }
            }
        });
    }

    componentWillUnmount(){
        window.removeEventListener("keyup", event => {
            event.preventDefault();
            if(event.keyCode === 13){
                this.setState({ submitted: true });
                const { username, password } = this.state;
                if (username && password)
                {
                    this.props.dispatch(userActions.login(username, password));
                }
            }
        });
    }

    handleChange(e)
    {
        const { name, value } = e.target;
        this.setState({ [name]: value });
    }

    handleSubmit(e)
    {
        e.preventDefault();

        this.setState({ submitted: true });
        const { username, password } = this.state;
        if (username && password)
        {
            this.props.dispatch(userActions.login(username, password));
        }
    }

    render()
    {
        const { alert } = this.props;
        const { username, password, submitted } = this.state;
        return (
            <div className="col-md-6 col-md-offset-3">
                <h2>Login</h2>
                {alert.message &&
                    <div className={`alert ${alert.type}`}>{alert.message}</div>
                }
                <form name="form" onSubmit={this.handleSubmit}>
                    <div className={'form-group' + (submitted && !username ? ' has-error' : '')}>
                        <label htmlFor="username">Username</label>
                        <input type="text" className="form-control" name="username" value={username} onChange={this.handleChange} />
                        {submitted && !username &&
                            <div className="help-block">Username is required</div>
                        }
                    </div>
                    <div className={'form-group' + (submitted && !password ? ' has-error' : '')}>
                        <label htmlFor="password">Password</label>
                        <input type="password" className="form-control" name="password" value={password} onChange={this.handleChange} />
                        {submitted && !password &&
                            <div className="help-block">Password is required</div>
                        }
                    </div>
                </form>
                <div className="form-group">
                    <button className="btn btn-primary" onClick={this.handleSubmit}>Login</button>
                    <button className='btn btn-success' onClick={() => { history.push('/register') }}>Sign Up </button>
                </div>
            </div >
        );
    }
}

function mapStateToProps(state)
{
    const { alert } = state;
    return {
        alert
    };
}

const connectedLoginPage = connect(mapStateToProps)(LoginPage);
export { connectedLoginPage as LoginPage }; 