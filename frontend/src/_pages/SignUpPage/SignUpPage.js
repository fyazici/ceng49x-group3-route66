import React, { Component } from 'react';
import { connect } from 'react-redux';
import { history } from '../../_helpers';
import { alertActions, userActions } from '../../_actions';

import './SignUpPage.css';

class SignUpPage extends Component
{
    constructor(props)
    {
        super(props);
        const { dispatch } = this.props;

        history.listen((location, action) =>
        {
            // Clear alert on location change. Meaning, if we submit wrong user name or password and reflesh the page
            // or went another url and come back after there should be no error message.
            dispatch(alertActions.clear());
        });

        this.state = {
            username: '',
            password: '',
            password2: '',
            name: '',
            surname: '',
            submitted: false
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount(){
        window.addEventListener("keyup", event => {
            event.preventDefault();
            if(event.keyCode === 13){
                this.setState({ submitted: true });
                const { username, password, password2, name, surname } = this.state;
                if (username && password && name && surname && (password === password2))
                {
                    this.props.dispatch(userActions.signUp(username, password, name, surname));
                }
            }
        });
    }

    componentWillUnmount(){
        window.removeEventListener("keyup", event => {
            event.preventDefault();
            if(event.keyCode === 13){
                this.setState({ submitted: true });
                const { username, password, password2, name, surname } = this.state;
                if (username && password && name && surname && (password === password2))
                {
                    this.props.dispatch(userActions.signUp(username, password, name, surname));
                }
            }
        });
    }

    handleChange(e)
    {
        const { name, value } = e.target;
        this.setState({ [name]: value });
    }

    handleSubmit(e)
    {
        e.preventDefault();

        this.setState({ submitted: true });
        const { username, password, password2, name, surname } = this.state;
        if (username && password && name && surname && (password === password2))
        {
            this.props.dispatch(userActions.signUp(username, password, name, surname));
        }
    }

    render()
    {
        const { alert } = this.props;
        const { username, password, password2, name, surname, submitted } = this.state;
        return (
            <div className="col-md-6 col-md-offset-3">
                <h2>SignUp</h2>
                {alert.message &&
                    <div className={`alert ${alert.type}`}>{alert.message}</div>
                }
                <form name="form" onSubmit={this.handleSubmit}>
                    <div className={'form-group' + (submitted && !name ? ' has-error' : '')}>
                        <label htmlFor="name">Name</label>
                        <input type="text" className="form-control" name="name" value={name} onChange={this.handleChange} />
                        {submitted && !name &&
                            <div className="help-block">Name is required</div>
                        }
                    </div>
                    <div className={'form-group' + (submitted && !surname ? ' has-error' : '')}>
                        <label htmlFor="surname">Surname</label>
                        <input type="text" className="form-control" name="surname" value={surname} onChange={this.handleChange} />
                        {submitted && !surname &&
                            <div className="help-block">Surname is required</div>
                        }
                    </div>
                    <div className={'form-group' + (submitted && !username ? ' has-error' : '')}>
                        <label htmlFor="username">Username</label>
                        <input type="text" className="form-control" name="username" value={username} onChange={this.handleChange} />
                        {submitted && !username &&
                            <div className="help-block">Username is required</div>
                        }
                    </div>
                    <div className={'form-group' + (submitted && !password ? ' has-error' : '')}>
                        <label htmlFor="password">Password</label>
                        <input type="password" className="form-control" name="password" value={password} onChange={this.handleChange} />
                        {submitted && !password && !password2 &&
                            <div className="help-block">Password is required</div>
                        }
                    </div>
                    <div className={'form-group' + (submitted && !password2 ? ' has-error' : '')}>
                        <label htmlFor="password">Type the Password Again</label>
                        <input type="password" className="form-control" name="password2" value={password2} onChange={this.handleChange} />
                        {submitted && password !== password2 &&
                            <div className="help-block">Passwords are not matched</div>
                        }
                    </div>
                </form>
                <div className="form-group">
                    <button className='btn btn-success' onClick={this.handleSubmit}>Sign Up </button>
                    <button className="btn btn-primary" onClick={() => { history.push('/login') }}>Back to Login</button>
                </div>
            </div >
        );
    }
}

function mapStateToProps(state)
{
    const { alert } = state;
    return {
        alert
    };
}

const connectedSignUpPage = connect(mapStateToProps)(SignUpPage);
export { connectedSignUpPage as SignUpPage }; 