import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Dropdown, DropdownButton} from 'react-bootstrap';

import { taskActions } from '../../_actions';

class TaskLine extends Component
{
    constructor(props)
    {
        super(props);

        this.camconf_task_id = -1;

        this.watchVideo = this.watchVideo.bind(this);
        this.watchDepth = this.watchDepth.bind(this);
        this.findTaskWithID = this.findTaskWithID.bind(this);
    }


    watchVideo()
    {
        const { renderer }= this.props.task.renderer_params
        
        switch (renderer)
        {
            case "cesiumjs":
                this.watchCesium();
                break;
            case "earthpro":
                this.watchEarthPro();
                break;
            case "fogcloud":
                this.watchFog();
                break;
            default:
                console.log("Unknown Renderer: ",renderer);
        }
    }

    watchCesium()
    {
        const { task } = this.props;
        this.props.dispatch(taskActions.selectAndWatchSolo(task.task_id  + "/cesium.mp4", task))
    }

    watchEarthPro()
    {
        const { task } = this.props;
        this.props.dispatch(taskActions.selectAndWatchSolo(task.task_id + "/earthpro.mp4", task))
    }

    watchDepth(type)
    {
        const { task } = this.props;
        const {taskList} = this.props.taskData;

        this.camconf_task_id = taskList[this.props.index].renderer_params.camconf_task_id;
        const dependencyTask = taskList.find(this.findTaskWithID);
        const dependencyTaskRenderer = dependencyTask.renderer_params.renderer;

        switch (dependencyTaskRenderer)
        {
            case "cesiumjs":
                //console.log(this.camconf_task_id + "/cesium.mp4", task.task_id + type )
                this.props.dispatch(taskActions.selectAndWatchDuo(this.camconf_task_id + "/cesium.mp4", task.task_id + type, task, dependencyTask ))
                break;
            case "earthpro":
                //console.log(this.camconf_task_id + "/earthpro.mp4", task.task_id + type )
                this.props.dispatch(taskActions.selectAndWatchDuo(this.camconf_task_id + "/earthpro.mp4", task.task_id + type, task, dependencyTask))
                break;
            default:
                console.log("Unknown Renderer: ", dependencyTaskRenderer);
        }
    }


    watchFog()
    {
        const { task } = this.props;
        const {taskList} = this.props.taskData;
        
        this.camconf_task_id = taskList[this.props.index].renderer_params.original_task_id;
        const dependencyTask = taskList.find(this.findTaskWithID);
        const dependencyTaskRenderer = dependencyTask.renderer_params.renderer;

        switch (dependencyTaskRenderer)
        {
            case "cesiumjs":
                //console.log(this.camconf_task_id + "/cesium.mp4", task.task_id + "/fog.mp4")
                this.props.dispatch(taskActions.selectAndWatchDuo(this.camconf_task_id + "/cesium.mp4", task.task_id + "/fog.mp4", task, dependencyTask));
                break;
            case "earthpro":
                //console.log(this.camconf_task_id + "/earthpro.mp4", task.task_id + "/fog.mp4")
                this.props.dispatch(taskActions.selectAndWatchDuo(this.camconf_task_id + "/earthpro.mp4", task.task_id + "/fog.mp4", task, dependencyTask));
                break;
            default:
                console.log("Unknown Renderer: ", dependencyTaskRenderer);
        }
    }

    findTaskWithID(task)
    {
        return task.task_id === this.camconf_task_id;
    }

    render()
    {
        const { task } = this.props;
        const { renderer } = task.renderer_params;
        return(
            <tr key={task.task_id}>
            <th scope="row" title={JSON.stringify(task)}>{task.task_id}</th>
            <td title={JSON.stringify(task)}>{task.name}</td>
            <td title={JSON.stringify(task)}>{task.status.toUpperCase()}</td>
            <td>
            { task.status === "finished" 
                ?
                (renderer === "depth" 
                    ?
                    <DropdownButton id="dropdown-basic-button" title="Watch">
                        <Dropdown.Item onClick={() => this.watchDepth("/altitude.mp4")}>Altitude</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.watchDepth("/depth.mp4")}>Depth</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.watchDepth("/valid.mp4")}>Valid</Dropdown.Item>
                    </DropdownButton>
                    :
                    (renderer === "cesiumjs" && task.renderer_params.dry_run === 1
                        ?
                        <button 
                            type="button"
                            disabled={true}
                            title="CesiumJS DryRun can not be played." 
                            className="btn btn-primary">
                                Watch
                        </button> 
                        :
                        <button 
                            type="button"
                            className="btn btn-primary" 
                            onClick={this.watchVideo}
                            >
                                Watch
                        </button>
                    )
                )
                :
                <button 
                    type="button"
                    disabled={true}
                    title="Not Finished" 
                    className="btn btn-primary">
                        Watch
                </button> 
            }
            </td>
        </tr>
        )
    }

}


function mapStateToProps(state)
{
    const { taskData } = state;
    return {
        taskData
    };
}


const connectedTaskLine = connect(mapStateToProps)(TaskLine);
export { connectedTaskLine as TaskLine };