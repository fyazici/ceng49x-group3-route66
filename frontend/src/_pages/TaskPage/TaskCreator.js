import React, {Component} from 'react';
import axios from 'axios';
import {Dropdown, DropdownButton} from 'react-bootstrap';

import CesiumJSTaskCreator from './taskCreationPages/cesiumjs';
import GoogleEarthProTaskCreator from './taskCreationPages/earthpro';
import DepthTaskCreator from './taskCreationPages/depth';
import FogCloudTaskCreator from './taskCreationPages/fog';

import { apiConstants } from '../../_constants';

import './taskCreator.css';
import 'react-widgets/dist/css/react-widgets.css';


export default class TaskCreator extends Component{

    constructor(props)
    {
        super(props);

        this.state = {
            isSelected: false,
            renderer: null,
            showInfo: false,
            message: "",
        };

        this.changeRenderer = this.changeRenderer.bind(this);
        this.submitTask = this.submitTask.bind(this);
    }

    changeRenderer(rendererName){
        this.setState({
            isSelected:true, 
            renderer: rendererName,
            showInfo: false,
            message: ""
        });
    }

    submitTask(taskBody){
        console.log("taskbody",JSON.stringify(taskBody));
        axios.post(apiConstants.BASE_URL + "/create-task",taskBody)
        .then( response => {
            if (response.status === 200)
            {
                if(response.data.message === "success"){
                    this.props.changeTaskMode(true);
                    return;
                }
                else{
                    this.setState({showInfo: true, message: "Task Creation Failed ! " + response.data.message});
                    return;
                }
            }
            this.setState({showInfo: true, message: "Task Creation Failed !"});
        })
        .catch( error => {
            this.setState({showInfo: true, message: "Task Creation Failed ! " + error});
        });
        
    }

    taskCreationForm(){
        switch (this.state.renderer)
        {
            case "CesiumJs":
                return <CesiumJSTaskCreator submitTask={this.submitTask} />;
            case "Google Earth Pro":
                return <GoogleEarthProTaskCreator submitTask={this.submitTask} />;
            case "Depth":
                return <DepthTaskCreator submitTask={this.submitTask} />
            case "Fog/Cloud":
                return <FogCloudTaskCreator submitTask={this.submitTask} />;
            default:
                return <div/>;
        }
    }

    render(){
        const {isSelected, renderer, showInfo, message } = this.state;
        return (
            <div className="taskForm">
                <h2 style={{ textAlign: 'center', marginBottom: '25px' }}> Create New Task </h2>
                <div className="renderSelectDiv" style={{ textAlign: 'center', alignItems: 'center', marginBottom: '15px' }}>
                    <DropdownButton id="dropdown-basic-button" variant="success" title={ isSelected ? renderer : "Select Renderer"}>
                        <Dropdown.Item onClick={() => this.changeRenderer("CesiumJs")}>CesiumJS</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.changeRenderer("Google Earth Pro")}>GoogleEarthPro</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.changeRenderer("Depth")}>Depth</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.changeRenderer("Fog/Cloud")}>Fog/Cloud</Dropdown.Item>
                    </DropdownButton>
                    {showInfo && 
                    <p style={{ textAlign: 'center', marginTop: '15px', fontWeight: "500", color:"darkred" }}>{message}</p>}
                </div>
                {this.taskCreationForm()}
            </div>
        );
    }
}