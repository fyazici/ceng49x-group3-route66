import React, {Component} from 'react';
import {connect} from 'react-redux';

import { dataActions } from '../../_actions';

import TaskCreator from './TaskCreator';
import { TaskLine } from './taskLine';

class TaskPage extends Component
{

    constructor(props)
    {
        super(props);

        this.state = {
            isAddTaskMode : false,
            showInfo: false
        };

        this.changeTaskMode = this.changeTaskMode.bind(this);
    }


    componentDidMount()
    {
        this.retrieveData();
    }

    changeTaskMode(showInfo)
    {
        showInfo && this.retrieveData();
        this.setState({isAddTaskMode: !this.state.isAddTaskMode, showInfo: showInfo});
    }

    retrieveData(){
        this.props.dispatch(dataActions.getUserLogs());
        this.props.dispatch(dataActions.getUserTasks());
    }

    render()
    {
        const { isAddTaskMode, showInfo } = this.state;
        return (
            <div className="taskPage">
                <div className="taskViewChangeButton">
                    <button onClick={() => this.changeTaskMode(false)}> {!isAddTaskMode ? "Add Task" : "View Tasks"}  </button>
                </div>
            {isAddTaskMode ? 
                <div> <TaskCreator changeTaskMode={this.changeTaskMode}/> </div>
                : 
                <div>
                    {showInfo && 
                    <p style={{  marginTop: '15px', fontWeight: "500", color:"darkblue" }}> - Task Created Successfully.</p>}
                    <h2 style={{ textAlign: 'center', marginBottom: '15px' }}> My Tasks </h2>
                    <table className="table">
                        <thead className="thead-dark">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">TaskName</th>
                                <th scope="col">Status</th>
                                <th scope="col">Watch</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                            this.props.taskData.loading 
                            ?
                                (<tr><td colSpan={4}>Loading</td></tr>)
                            :
                                (this.props.taskData.isRetrieved 
                                    &&
                                    (
                                        (this.props.taskData.taskList.length < 1)
                                        ?
                                            (<tr><td colSpan={4}>No Task Found</td></tr>)
                                        :
                                            (this.props.taskData.taskList.map( (task, index) => (
                                                <TaskLine key={index} task={task} index={index}/>
                                                ))
                                            )
                                    )
                                )
                            }
                        </tbody>
                    </table>
                <div />
            </div>
            }
            </div>
        )
    }
}

function mapStateToProps(state)
{
    const { taskData } = state;
    return {
        taskData
    };
}

const connectedTaskPage = connect(mapStateToProps)(TaskPage);
export { connectedTaskPage as TaskPage };