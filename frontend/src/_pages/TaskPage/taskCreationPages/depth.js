import React from 'react';
import { useForm } from 'react-hook-form';

export default function DepthTaskCreator(props) {
    const { register, handleSubmit, errors, setValue } = useForm();

    const onSubmit = data => {
        
        const dependendTask = findTask(parseInt(data.task_id));
        const frustum_near_val = dependendTask.renderer_params.renderer === "cesiumjs" ? 
                                        dependendTask.renderer_params.frustum_near : 1;
        const requestBody = 
        {
            "token":sessionStorage.getItem('user'),
            "task_name":data.task_name,
            "dependencies":[parseInt(data.task_id)],
            "log_file_id":parseInt(dependendTask.log_file_id),
            "task_info":{
                "width":parseInt(dependendTask.width),
                "height":parseInt(dependendTask.height),
                "fov":parseInt(dependendTask.fov),
                "framerate":parseInt(dependendTask.framerate),
                "camera_pan":parseInt(dependendTask.camera_pan),
                "camera_tilt":parseInt(dependendTask.camera_tilt),
                "camera_roll":parseInt(dependendTask.camera_roll),
                "renderer_params":{
                    "time_offset":parseInt(dependendTask.time_offset),
                    "logmap_zero":parseFloat(data.logmap_zero),
                    "camconf_task_id":parseInt(dependendTask.task_id),
                    "renderer":data.renderer, // "depth",
                    "frustum_near":parseFloat(frustum_near_val)
                }
            }
        }
        props.submitTask(requestBody);
    };

    const findTask = task_id =>
    {   
        const compareWithId = task => {
            return task.task_id === task_id;
        }
        return JSON.parse(sessionStorage.getItem('tasks')).find(compareWithId);
    }
    
    const findTasksForDepthVideo = task =>
    {
        return task.status === "finished" && (task.renderer_params.renderer === "cesiumjs");
    }

    const taskList = JSON.parse(sessionStorage.getItem('tasks')).filter(findTasksForDepthVideo);

    const setDefaultValue = (e) => {
        if(e.target.checked){
            setValue("logmap_zero", 100);
        }
        else{
            setValue("logmap_zero", "");
        }
    }

    return (
        <form onSubmit={handleSubmit(onSubmit)}>

            <select hidden={true} className="Renderer" name="renderer" ref={register()}>
                <option value="depth">depth</option>
            </select>

            <div className="flexbox">
            <h6>Fill with default: </h6>
                <div className="checkBoxDiv">
                    <input type="checkbox" onChange={setDefaultValue} name="fillWithDefault" ref={register()} />
                </div>
            </div>

            <input
                type="text"
                placeholder="Task Name"
                title="Task Name" 
                name="task_name"
                ref={register({ required: true, min: 1 })}
            />
            {errors.task_name && <p>Task name must be specified.</p>}

            <div className="selectBox" title="Only Finished Cesium or EarthPro Task can be selected.">
                <h6>Task to Create Depth Video</h6>
                <div className="selectLogDiv">
                    <select
                        type="number"
                        placeholder="Task for Depth Video"
                        name="task_id"
                        ref={register({ required: true })}
                    >
                        <option value="">Select a Task</option> 
                        {taskList.map( (task, index) => 
                            <option key={index} value={task.task_id}>{task.task_id + " " + task.name}</option> 
                            )}
                    </select>
                </div>
            </div>
            {errors.task_id && <p>Task must be selected.</p>}

            <input
                type="number"
                placeholder="LogMap Zero"
                title="LogMap Zero"
                name="logmap_zero"
                ref={register({ required: true, min:1, max:60000 })}
            />
            {errors.logmap_zero && 
                (errors.logmap_zero.type ==="required" 
                ? 
                <p>LogMap Zero value must be specified.</p>
                :
                <p>LogMap Zero value must be in between 1 and 60000</p>
                )
            }

            <input className="submitButton" type="submit" value="SUBMIT" />
        </form>
  );
}