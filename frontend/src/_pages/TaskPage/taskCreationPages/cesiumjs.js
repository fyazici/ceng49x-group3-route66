import React from 'react';
import DateTimePicker from 'react-widgets/lib/DateTimePicker'
import { useForm } from 'react-hook-form';

export default function CesiumJSTaskCreator(props) {
    const { register, handleSubmit, errors, setValue, watch } = useForm();
    
    let selectedDate = null;
    const isDate = watch("time_offset")

    const onSubmit = data => {
        const requestBody = 
        {
            "token":sessionStorage.getItem('user'),
            "task_name":data.task_name,
            "dependencies":[],
            "log_file_id":parseInt(data.log_file_id),
            "task_info":{
                "width":parseInt(data.width),
                "height":parseInt(data.height),
                "fov":parseInt(data.fov),
                "framerate":parseInt(data.frameRate),
                "camera_pan":parseInt(data.camera_pan),
                "camera_tilt":parseInt(data.camera_tilt),
                "camera_roll":parseInt(data.camera_roll),
                "renderer_params":{
                    "time_offset":parseInt(isDate ? (selectedDate || (new Date()).getTime() ) : (new Date()).getTime())-3*3600*1000,
                    "dry_run":data.dry_run ? 1 : 0,
                    "renderer":data.renderer, // "cesiumjs",
                    "skybox":data.skybox ? 1 : 0,
                    "lighting_mode":data.lighting_mode ? 1 : 0,
                    "light_fade_dist":parseInt(data.light_fade_dist),
                    "night_fade_dist":parseInt(data.night_fade_dist),
                    "shadows":data.shadows ? 1 : 0,
                    "terrain_shadow_mode":parseInt(data.terrain_shadow_mode),
                    "show_ground_atm_mode":data.show_ground_atm_mode ? 1 : 0,
                    "terrain_depth_test":data.terrain_depth_test ? 1 : 0,
                    "time_advance_mode":data.time_advance_mode ? 1 : 0,
                    "water_mask":data.water_mask ? 1 : 0,
                    "vertex_normals":data.vertex_normals ? 1 : 0,
                    "frustum_near":parseFloat(data.frustum_near),
                    "frustum_far":parseFloat(data.frustum_far)
                }
            }
        }
        props.submitTask(requestBody);
    };

    const setDefaultValue = (e) => {
        if(e.target.checked){
            setValue("width",1920);
            setValue("height",1080);
            setValue("fov",90);
            setValue("frameRate",30);
            setValue("camera_pan",0);
            setValue("camera_tilt",0);
            setValue("camera_roll",0);
            setValue("time_offset",0);
            setValue("light_fade_dist",10000000);
            setValue("night_fade_dist",10000000);
            setValue("frustum_near",1);
            setValue("frustum_far",500000000);
            setValue("time_offset",0);
        }
        else{
            setValue("width", "");
            setValue("height", "");
            setValue("fov", "");
            setValue("frameRate", "");
            setValue("camera_pan", "");
            setValue("camera_tilt", "");
            setValue("camera_roll", "");
            setValue("time_offset", "");
            setValue("light_fade_dist", "");
            setValue("night_fade_dist", "");
            setValue("frustum_near", "");
            setValue("frustum_far", "");
        }
        setValue("shadows",e.target.checked);
        setValue("skybox",e.target.checked);
        setValue("lighting_mode",e.target.checked);
        setValue("show_ground_atm_mode",e.target.checked);
        setValue("terrain_depth_test",e.target.checked);
        setValue("water_mask",e.target.checked);
        setValue("vertex_normals",e.target.checked);
    }
    
    return (
        <form onSubmit={handleSubmit(onSubmit)}>

            <select hidden={true} className="Renderer" name="renderer" ref={register()}>
                <option value="cesiumjs">cesiumjS</option>
            </select>

            <div className="flexbox">
            <h6>Fill with default: </h6>
                <div className="checkBoxDiv">
                    <input type="checkbox" onChange={setDefaultValue} name="fillWithDefault" ref={register()} />
                </div>
            </div>

            <input
                type="text"
                placeholder="Task Name"
                title={"Task Name"}
                name="task_name"
                ref={register({ required: true, min: 1 })}
            />
            {errors.task_name && <p>Task name must be specified.</p>}

            <div className="selectBox">
                <h6>LogFile</h6>
                <div className="selectLogDiv">
                    <select
                        type="number"
                        placeholder="Log File ID"
                        name="log_file_id"
                        ref={register({ required: true })}
                    >
                        <option value="">Select a log file</option> 
                        {JSON.parse(sessionStorage.getItem('logs')).map( log =>
                            <option key={log.log_file_id} value={log.log_file_id}>{log.log_file_id + " " + log.log_file_name}</option> 
                            )}
                    </select>
                </div>
            </div>
            {errors.log_file_id && <p>Log file must be selected.</p>}
            
            <div className="dual">
                <input
                    type="number"
                    placeholder="Width"
                    title={"Width"}
                    name="width"
                    ref={register({ required: true, min:100, max:10000 })}
                />

                <input
                    type="number"
                    placeholder="Height"
                    title={"Height"}
                    name="height"
                    ref={register({ required: true, min:100, max:10000 })}
                />
                {(errors.width || errors.height) && 
                ((      (errors.width && errors.width.type ==="required") 
                    ||  (errors.height && errors.height.type ==="required")) 
                ? 
                <p>Width and Height must be specified.</p>
                :
                <p>Width and Height must be in between 100 and 10000</p>
                )
                }
            </div>

            <input
                type="number"
                placeholder="Fov"
                title={"Fov"}
                name="fov"
                ref={register({ required: true, min:1, max:179 })}
            />
            {errors.fov && 
                (errors.fov.type ==="required" 
                ? 
                <p>Fov must be specified. </p>
                :
                <p>Fov must be in between 0 and 180</p>
                )
            }

            <input
                    type="number"
                    placeholder="Frame Rate"
                    title={"Frame Rate"}
                    name="frameRate"
                    ref={register({ required: true, min:1 })}
                />
            {errors.frameRate && 
                (errors.frameRate.type ==="required" 
                ? 
                <p>Frame Rate must be specified. </p>
                :
                <p>Frame Rate must be higher than 0.</p>
                )
            }
            
            <div className="triple">
                <input
                    type="number"
                    placeholder="Camera Pan"
                    title={"Camera Pan"}
                    name="camera_pan"
                    ref={register({ required: true, min:0, max:360 })}
                />

                <input
                    type="number"
                    placeholder="Camera Tilt"
                    title={"Camera Tilt"}
                    name="camera_tilt"
                    ref={register({ required: true, min:-90, max:90 })}
                />

                <input
                    type="number"
                    placeholder="Camera Roll"
                    title={"Camera Roll"}
                    name="camera_roll"
                    ref={register({ required: true, min:0, max:360 })}
                />
                {(errors.camera_pan || errors.camera_tilt || errors.camera_roll) && 
                ((      (errors.camera_pan  && errors.camera_pan.type ==="required") 
                    ||  (errors.camera_tilt && errors.camera_tilt.type ==="required") 
                    ||  (errors.camera_roll && errors.camera_roll.type ==="required")) 
                ? 
                <p>Camera angles must be specified.</p>
                :
                <p>Camera angles must be in between [0, 360] for Pan, [-90, 90] for Tilt and [0, 360] for Roll.</p>
                )
                }
            </div>

            <div className="flexbox" style={{margin:0, height:"38px"}}>
                <h6 style={{marginTop:"10px"}}>Time Offset</h6>
                <div className="checkBoxDiv" style={{marginTop:"3px"}}>
                    <input
                        type="checkbox"
                        className="checkBoxForTimeOffset"
                        name="time_offset"
                        ref={register({})}
                    />
                </div>
                <div className="datePicker">
                {isDate && 
                    <DateTimePicker
                    timePrecision= 'seconds'
                    dropUp = {true}
                    defaultValue={new Date()}
                    onChange={(date)=> {selectedDate = date.getTime()}} />}
                </div>
            </div>

            <div className="flexbox" style={{marginTop:0}}>
                <h6>DryRun</h6>
                <div className="checkBoxDiv">
                    <input
                        type="checkbox"
                        placeholder="DryRun"
                        name="dry_run"
                        ref={register({})}
                    />
                </div>
            </div>

            <div className="flexbox">
                <h6>Skybox</h6>
                <div className="checkBoxDiv">
                    <input
                        type="checkbox"
                        placeholder="Skybox"
                        name="skybox"
                        ref={register({})}
                    />
                </div>
            </div>

            <div className="selectBox">
                <h6>Lighting Mode </h6>
                <div className="checkBoxDiv">
                    <input
                        type="checkbox"
                        placeholder="lighting_mode"
                        name="lighting_mode"
                        ref={register({})}
                    />
                </div>
            </div>

            <div className="dual">
                <input
                    type="number"
                    placeholder="Light Fade Dist"
                    title={"Light Fade Dist"}
                    name="light_fade_dist"
                    ref={register({ required: true, min:1 })}
                />

                <input
                    type="number"
                    placeholder="Night Fade Dist"
                    title={"Night Fade Dist"}
                    name="night_fade_dist"
                    ref={register({ required: true, min:1 })}
                />
                {(errors.light_fade_dist || errors.night_fade_dist) && 
                ((      (errors.light_fade_dist && errors.light_fade_dist.type ==="required") 
                    ||  (errors.night_fade_dist && errors.night_fade_dist.type ==="required")) 
                ? 
                <p>Fade values must be specified.</p>
                :
                <p>Fade values must be higher than 0.</p>
                )
                }
            </div>

            <div className="flexbox">
                <h6>Shadows</h6>
                <div className="checkBoxDiv">
                    <input
                        type="checkbox"
                        placeholder="shadows"
                        name="shadows"
                        ref={register({})}
                    />
                </div>
            </div>

            <div className="selectBox">
                <h6>Terrain Shadow Mode</h6>
                <div className="selectShadowDiv">
                    <select name="terrain_shadow_mode" defaultValue={"1"} ref={register({ required: true })}>
                        <option value="0">Off</option>
                        <option value="1">Receive Only</option>
                        <option value="2">Cast Only</option>
                        <option value="3">Receive and Cast</option>
                    </select>
                </div>
            </div>

            <div className="flexbox">
                <h6>Show Ground ATM Mode</h6>
                <div className="checkBoxDiv">
                    <input
                        type="checkbox"
                        placeholder="show_ground_atm_mode"
                        name="show_ground_atm_mode"
                        ref={register({})}
                    />
                </div>
            </div>

            <div className="flexbox">
                <h6>Terrain Depth Test</h6>
                <div className="checkBoxDiv">
                    <input
                        type="checkbox"
                        placeholder="terrain_depth_test"
                        name="terrain_depth_test"
                        ref={register({})}
                    />
                </div>
            </div>

            <div className="flexbox">
                <h6> Time Advance Mode </h6>
                <div className="checkBoxDiv">
                    <input
                        type="checkbox"
                        placeholder="time_advance_mode"
                        name="time_advance_mode"
                        ref={register({})}
                    />          
                </div>
            </div>

            <div className="flexbox">
                <h6> Water Mask </h6>
                <div className="checkBoxDiv">
                    <input
                        type="checkbox"
                        placeholder="water_mask"
                        name="water_mask"
                        ref={register({})}
                    />
                </div>
            </div>

            <div className="flexbox">
                <h6> Vertex Normals </h6>
                <div className="checkBoxDiv">
                    <input
                        type="checkbox"
                        placeholder="vertex_normals"
                        name="vertex_normals"
                        ref={register({})}
                    />
                </div>
            </div>
            
            <div className="dual">
                <input
                    type="number"
                    placeholder="Frustum Near"
                    title={"Frustum Near"}
                    name="frustum_near"
                    ref={register({ required: true, min:1 })}
                />
                <input
                    type="number"
                    placeholder="Frustum Far"
                    name="frustum_far"
                    title={"Frustum Far"}
                    ref={register({ required: true, min:1 })}
                />
                {(errors.frustum_near || errors.frustum_far) && 
                ((      (errors.frustum_near && errors.frustum_near.type ==="required") 
                    ||  (errors.frustum_far && errors.frustum_far.type ==="required")) 
                ? 
                <p>Frustum values must be specified.</p>
                :
                <p>Frustum values must be higher than 0.</p>
                )
                }
            </div>
            <input className="submitButton" type="submit" value="SUBMIT" />
        </form>
  );
}