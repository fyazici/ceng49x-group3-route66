import React from 'react';
import DateTimePicker from 'react-widgets/lib/DateTimePicker'
import { useForm } from 'react-hook-form';

export default function GoogleEarthProTaskCreator(props) {
    const { register, handleSubmit, errors, setValue, watch } = useForm();

    let selectedDate = null;
    const isDate = watch("time_offset")

    const onSubmit = data => {
    const requestBody = 
        {
            "token":sessionStorage.getItem('user'),
            "task_name":data.task_name,
            "dependencies":[],
            "log_file_id":parseInt(data.log_file_id),
            "task_info":{
                "width":parseInt(data.width),
                "height":parseInt(data.height),
                "fov":parseInt(data.fov),
                "framerate":parseInt(data.frameRate),
                "camera_pan":parseInt(data.camera_pan),
                "camera_tilt":parseInt(data.camera_tilt),
                "camera_roll":parseInt(data.camera_roll),
                "renderer_params":{
                    "time_offset":parseInt(isDate ? (selectedDate || (new Date()).getTime() ) : (new Date()).getTime())-3*3600*1000,
                    "renderer":data.renderer, // "earthpro",
                }
            }
        }
        props.submitTask(requestBody);
    };

    const setDefaultValue = (e) => {
        if(e.target.checked){
            setValue("width",1920);
            setValue("height",1080);
            setValue("fov",90);
            setValue("frameRate",30);
            setValue("camera_pan",0);
            setValue("camera_tilt",0);
            setValue("camera_roll",0);
            setValue("time_offset",0);
        }
        else{
            setValue("width", "");
            setValue("height", "");
            setValue("fov", "");
            setValue("frameRate", "");
            setValue("camera_pan", "");
            setValue("camera_tilt", "");
            setValue("camera_roll", "");
        }
    }

    return (
        <form onSubmit={handleSubmit(onSubmit)}>

            <select hidden={true} className="Renderer" name="renderer" ref={register()}>
                <option value="earthpro">earthpro</option>
            </select>

            <div className="flexbox">
            <h6>Fill with default: </h6>
                <div className="checkBoxDiv">
                    <input type="checkbox" onChange={setDefaultValue} name="fillWithDefault" ref={register()} />
                </div>
            </div>

            <input
                type="text"
                placeholder="Task Name"
                title="Task Name"
                name="task_name"
                ref={register({ required: true, min: 1 })}
            />
            {errors.task_name && <p>Task name must be specified.</p>}

            <div className="selectBox">
                <h6>LogFile</h6>
                <div className="selectLogDiv">
                    <select
                        type="number"
                        placeholder="Log File ID"
                        name="log_file_id"
                        ref={register({ required: true })}
                    >
                        <option value="">Select a log file</option> 
                        {JSON.parse(sessionStorage.getItem('logs')).map( log =>
                            <option key={log.log_file_id} value={log.log_file_id}>{log.log_file_id + " " + log.log_file_name}</option> 
                            )}
                    </select>
                </div>
            </div>
            {errors.log_file_id && <p>Log file must be selected.</p>}
            
            <div className="dual">
                <input
                    type="number"
                    placeholder="Width"
                    title={"Width"}
                    name="width"
                    ref={register({ required: true, min:100, max:10000 })}
                />

                <input
                    type="number"
                    placeholder="Height"
                    title={"Height"}
                    name="height"
                    ref={register({ required: true, min:100, max:10000 })}
                />
                {(errors.width || errors.height) && 
                ((      (errors.width && errors.width.type ==="required") 
                    ||  (errors.height && errors.height.type ==="required")) 
                ? 
                <p>Width and Height must be specified.</p>
                :
                <p>Width and Height must be in between 100 and 10000</p>
                )
                }
            </div>

            <input
                    type="number"
                    placeholder="Frame Rate"
                    title={"Frame Rate"}
                    name="frameRate"
                    ref={register({ required: true, min:1 })}
                />
            {errors.frameRate && 
                (errors.frameRate.type ==="required" 
                ? 
                <p>Frame Rate must be specified. </p>
                :
                <p>Frame Rate must be higher than 0.</p>
                )
            }

            <div className="triple">
                <input
                    type="number"
                    placeholder="Camera Pan"
                    title={"Camera Pan"}
                    name="camera_pan"
                    ref={register({ required: true, min:0, max:360 })}
                />

                <input
                    type="number"
                    placeholder="Camera Tilt"
                    title={"Camera Tilt"}
                    name="camera_tilt"
                    ref={register({ required: true, min:-90, max:90 })}
                />

                <input
                    type="number"
                    placeholder="Camera Roll"
                    title={"Camera Roll"}
                    name="camera_roll"
                    ref={register({ required: true, min:0, max:360 })}
                />
                {(errors.camera_pan || errors.camera_tilt || errors.camera_roll) && 
                ((      (errors.camera_pan  && errors.camera_pan.type ==="required") 
                    ||  (errors.camera_tilt && errors.camera_tilt.type ==="required") 
                    ||  (errors.camera_roll && errors.camera_roll.type ==="required")) 
                ? 
                <p>Camera angles must be specified.</p>
                :
                <p>Camera angles must be in between [0, 360] for Pan, [-90, 90] for Tilt and [0, 360] for Roll.</p>
                )
                }
            </div>

            <input
                type="number"
                placeholder="Fov"
                title={"Fov"}
                name="fov"
                ref={register({ required: true, min:1, max:179 })}
            />
            {errors.fov && 
                (errors.fov.type ==="required" 
                ? 
                <p>Fov must be specified. </p>
                :
                <p>Fov must be in between 0 and 180</p>
                )
            }

            <div className="flexbox" style={{margin:"0 0 10px 0", height:"38px"}}>
                <h6 style={{marginTop:"10px"}}>Time Offset</h6>
                <div className="checkBoxDiv" style={{marginTop:"3px"}}>
                    <input
                        type="checkbox"
                        className="checkBoxForTimeOffset"
                        name="time_offset"
                        ref={register({})}
                    />
                </div>
                <div className="datePicker">
                {isDate && 
                    <DateTimePicker
                    timePrecision= 'seconds'
                    dropUp = {true}
                    defaultValue={new Date()}
                    onChange={(date)=> {selectedDate = date.getTime()}} />}
                </div>
            </div>
            
            <input className="submitButton" type="submit" value="SUBMIT" />
        </form>
  );
}