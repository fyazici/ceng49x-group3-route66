import React from 'react';
import { useForm } from 'react-hook-form';

export default function FogCloudTaskCreator(props) {
    const { register, handleSubmit, errors, watch, setValue } = useForm();
    
    const watchPerlin_k = watch("perlin_k", 0);
    const isPerlinK = watch("isPerlinK",0);
    const isColor = watch("isColor",0);

    const onSubmit = data => {
        
        const videoDependedTask = findTask(parseInt(data.task_id_video));
        const depthDependedTask = findTask(parseInt(data.task_id_depth));

        let perlin = {
            "perlin_frequency":[parseInt(data.perlin_frequency1),parseInt(data.perlin_frequency2),parseInt(data.perlin_frequency3)]
        }
        if(isPerlinK && isColor){
            perlin = {
                "perlin_frequency":[parseInt(data.perlin_frequency1),parseInt(data.perlin_frequency2),parseInt(data.perlin_frequency3)],
                "k":parseInt(data.perlin_k)/100,
                "color":data.fogCloudColor
            }
        }
        else if(isPerlinK){
            perlin = {
                "perlin_frequency":[parseInt(data.perlin_frequency1),parseInt(data.perlin_frequency2),parseInt(data.perlin_frequency3)],
                "k":parseInt(data.perlin_k)/100
            }
        }
        else if(isColor){
            perlin = {
                "perlin_frequency":[parseInt(data.perlin_frequency1),parseInt(data.perlin_frequency2),parseInt(data.perlin_frequency3)],
                "color":data.fogCloudColor
            }
        }

        const requestBody = 
        {
            "token":sessionStorage.getItem('user'),
            "task_name":data.task_name,
            "dependencies":[parseInt(videoDependedTask.task_id),parseInt(depthDependedTask.task_id)],
            "task_info":{
                "width":parseInt(videoDependedTask.width),
                "height":parseInt(videoDependedTask.height),
                "framerate":parseInt(videoDependedTask.framerate),
                "renderer_params":{
                    "renderer":data.renderer, // "fogcloud"
                    "depth_task_id":parseInt(data.task_id_depth),
                    "original_task_id":parseInt(data.task_id_video),
                    "logmap_zero": parseFloat(depthDependedTask.renderer_params.logmap_zero),
                    "options":{
                        "fog_factor":{
                            "type":data.fog_factor
                        },
                        "min_dist":parseInt(data.min_dist),
                        "max_dist":parseInt(data.max_dist),
                        "max_density":parseFloat(data.max_density),
                        "perlin":perlin
                    }
                }
            }
        }
        props.submitTask(requestBody);
    };

    const findTask = task_id =>
    {   
        const compareWithId = task => {
            return task.task_id === task_id;
        }
        return JSON.parse(sessionStorage.getItem('tasks')).find(compareWithId);
    }

    const findTasksForVideo = task =>
    {
        return task.status === "finished" && (
            (   task.renderer_params.renderer === "cesiumjs" && task.renderer_params.dry_run === 0  ) ||
                task.renderer_params.renderer === "earthpro");
    }
    
    const findTasksForDepthVideo = task =>
    {
        return task.status === "finished" && (task.renderer_params.renderer === "depth");
    }

    const taskListDepthVideo = JSON.parse(sessionStorage.getItem('tasks')).filter(findTasksForDepthVideo);
    const taskListVideo = JSON.parse(sessionStorage.getItem('tasks')).filter(findTasksForVideo);

    const setDefaultValue = (e) => {
        if(e.target.checked){
            setValue("fog_factor","exponential2");
            setValue("min_dist",100);
            setValue("max_dist",15000);
            setValue("max_density",1);
            setValue("perlin_frequency1",5);
            setValue("perlin_frequency2",5);
            setValue("perlin_frequency3",3);
        }
        else{
            setValue("fog_factor","");
            setValue("min_dist","");
            setValue("max_dist","");
            setValue("max_density","");
            setValue("perlin_frequency1","");
            setValue("perlin_frequency2","");
            setValue("perlin_frequency3","");
        }
    }

    return (
        <form onSubmit={handleSubmit(onSubmit)}>

            <select hidden={true} className="Renderer" name="renderer" ref={register()}>
                <option value="fogcloud">fogcloud</option>
            </select>

            <div className="flexbox">
            <h6>Fill with default: </h6>
                <div className="checkBoxDiv">
                    <input type="checkbox" onChange={setDefaultValue} name="fillWithDefault" ref={register()} />
                </div>
            </div>

            <input
                type="text"
                placeholder="Task Name"
                title="Task Name"
                name="task_name"
                ref={register({ required: true, min: 1 })}
            />
            {errors.task_name && <p>Task name must be specified.</p>}

            <div className="selectBox">
                <h6>Task for Video</h6>
                <div className="selectLogDiv">
                    <select
                        type="number"
                        placeholder="Task for Video"
                        name="task_id_video"
                        ref={register({ required: true })}
                    >
                        <option value="">Select a Task</option> 
                        {taskListVideo.map( (task, index) => 
                            <option key={index} value={task.task_id}>{task.task_id + " " + task.name}</option> 
                            )}
                    </select>
                </div>
            </div>
            {errors.task_id_video && <p>Task for Video must be selected.</p>}

            <div className="selectBox">
                <h6>Task for Depth Video</h6>
                <div className="selectLogDiv">
                    <select
                        type="number"
                        placeholder="Task for Depth Video"
                        name="task_id_depth"
                        ref={register({ required: true })}
                    >
                        <option value="">Select a Task</option> 
                        {taskListDepthVideo.map( (task, index) => 
                            <option key={index} value={task.task_id}>{task.task_id + " " + task.name}</option> 
                            )}
                    </select>
                </div>
            </div>
            {errors.task_id_depth && <p>Task for Depth Video must be selected.</p>}

            <div className="selectBox">
                <h6>Fog Factor Type</h6>
                <div className="selectLogDiv">
                    <select
                        type="number"
                        name="fog_factor"
                        ref={register({ required: true })}
                    >
                        <option value="">Select Fog Factor</option> 
                        <option value="exponential">Exponential</option> 
                        <option value="exponential2">Exponential2</option> 
                        <option value="linear">Linear</option> 
                    </select>
                </div>
            </div>
            {errors.fog_factor && <p>Fog Factor must be selected.</p>}

            <div className="triple">
                <input
                    type="number"
                    placeholder="Min. Distance"
                    title="Min. Distance"
                    name="min_dist"
                    ref={register({ required: true, min:1, max:1000 })}
                />
                <input
                    type="number"
                    placeholder="Max. Distance"
                    title="Max. Distance"
                    name="max_dist"
                    ref={register({ required: true, min:1, max:50000 })}
                />
                <input
                    type="number"
                    placeholder="Max. Density"
                    title="Max. Density"
                    name="max_density"
                    ref={register({ required: true, min:1, max:10 })}
                />
                {(errors.min_dist || errors.max_dist || errors.max_density) && 
                ((      (errors.min_dist && errors.min_dist.type === "required") 
                    ||  (errors.max_dist && errors.max_dist.type === "required") 
                    ||  (errors.max_density && errors.max_density.type === "required")
                )
                ? 
                <p>Min/Max distance and Density values must be specified.</p>
                :
                <p> Min Distance value must be between 1 and 1000.<br/>
                    Max Distance value must be between 1 and 50000.<br/>
                    Max Density value must be between 1 and 10.</p>
                )
                }
            </div>

            <div className="triple" title="Perlin Frequency Turn into [val1,val2,val3]">
                <h6 style={{marginBottom : "10px"}}>Perlin Frequency</h6>
                <input
                    type="number"
                    placeholder="val1"
                    title="Perlin Frequency val1"
                    name="perlin_frequency1"
                    ref={register({ required: true, min:1, max:10 })}
                />

                <input
                    type="number"
                    placeholder="val2"
                    title="Perlin Frequency val2"
                    name="perlin_frequency2"
                    ref={register({ required: true, min:1, max:10 })}
                />

                <input
                    type="number"
                    placeholder="val3"
                    title="Perlin Frequency val3"
                    name="perlin_frequency3"
                    ref={register({ required: true, min:1, max:10})}
                />
                {(errors.perlin_frequency1 || errors.perlin_frequency2 || errors.perlin_frequency3) && 
                ((      (errors.perlin_frequency1  && errors.perlin_frequency1.type ==="required") 
                    ||  (errors.perlin_frequency2 && errors.perlin_frequency2.type ==="required") 
                    ||  (errors.perlin_frequency3 && errors.perlin_frequency3.type ==="required")) 
                ? 
                <p>Perlin Frequency values must be specified.</p>
                :
                <p>Perlin Frequency values must be between 1 and 10.</p>
                )
                }
            </div>

            <div className="flexbox">
                <h6>Perlin K (Optional)</h6>
                <div className="checkBoxDiv">
                    <input type="checkbox" name="isPerlinK" ref={register()}/>
                </div>
            </div>

            {isPerlinK ? <div>
                <h6>Perlin K : {watchPerlin_k/100}</h6>
                <input
                    type="range"
                    placeholder="Perlin K"
                    name="perlin_k"
                    defaultValue = {0}
                    ref={register()}
                />
            </div>:""}

            <div className="flexbox">
                <h6>Color (Optional)</h6>
                <div className="checkBoxDiv">
                    <input type="checkbox" name="isColor" ref={register()}/>
                </div>
                {
                    isColor ?  
                    <input 
                        type="color"
                        className="colorSelect" 
                        name="fogCloudColor" 
                        defaultValue="#ffffff" 
                        ref={register()}
                    />
                    :""
                }
            </div>


            <input className="submitButton" type="submit" value="SUBMIT" />
        </form>
  );
}