export const taskConstants = {
    TASK_SELECTED_SOLO: 'TASK_SELECTED_SOLO',
    TASK_SELECTED_DUO: 'TASK_SELECTED_DUO',
};
