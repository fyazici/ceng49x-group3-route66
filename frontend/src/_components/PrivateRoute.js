import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

// This route helps us to prevent access just changing the URL.

const PrivateRoute = ({ component: Component, user, ...rest }) => (
    <Route {...rest} render={props => (
        user
            ? <Component {...props} />
            : <Redirect to={{ pathname: '/login', state: { from: props.location } }} />
    )} />
)

function mapStateToProps(state)
{
    const { authentication } = state;
    const { user } = authentication;
    return {
        user
    };
}

const connectedPrivateRoute = connect(mapStateToProps)(PrivateRoute);
export { connectedPrivateRoute as PrivateRoute };