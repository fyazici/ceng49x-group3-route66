import React, { Component } from 'react';
import { connect } from 'react-redux';
import Nav from 'react-bootstrap/Nav'
import Navbar from 'react-bootstrap/Navbar'
import { userActions } from '../../_actions';

class Header extends Component
{
    logout = () =>
    {
        this.props.dispatch(userActions.logout());
    }

    render()
    {
        return (
            <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
                <Navbar.Brand href="/tasks">Route66</Navbar.Brand>
                <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                <Navbar.Collapse id="responsive-navbar-nav">
                    <Nav className="mr-auto">
                        { this.props.loggedIn && <Nav.Link href="/tasks">Tasks</Nav.Link> }
                        { this.props.loggedIn && <Nav.Link href="/logs">Logs</Nav.Link> }
                    </Nav>
                    <Nav>
                        {this.props.loggedIn &&
                            <Nav.Link onClick={this.logout} > Logout </Nav.Link>
                        }
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
        );
    }
}

function mapStateToProps(state)
{
    const { authentication } = state;
    const { loggedIn } = authentication;
    return {
        loggedIn,
    };
}

const connectedHeader = connect(mapStateToProps)(Header);
export { connectedHeader as Header };