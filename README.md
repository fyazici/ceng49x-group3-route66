METU CENG49x Senior Project - Route66
==

This is our last year project for the Computer Engineering BSc degree.

Project webpage: [https://senior.ceng.metu.edu.tr/2020/route66/](https://senior.ceng.metu.edu.tr/2020/route66/)

Final project video: [https://youtu.be/AUNHlOPCG7o](https://youtu.be/AUNHlOPCG7o)