from flask import Flask, render_template, jsonify
from flask_cors import CORS

app = Flask(__name__, static_folder="templates/static", static_url_path="/static")
CORS(app)

@app.route("/", defaults={"path": ""})
@app.route("/<path:path>")
def catch_all(path):
  return render_template("index.html")

@app.route("/health-check")
def health_check():
    return jsonify(success=True)

if __name__ == "__main__":
    app.run(port=5555)
