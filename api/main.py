from flask import Flask,request,send_file,abort,jsonify, Response
from flask_cors import CORS


import json
import sqlite3
from sqlite3 import Error
import datetime


#sql alchemy
import sqlalchemy as db
from sqlalchemy import Table, Column, Integer, String, MetaData

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, scoped_session
from base import Base
from models_ import *
from serializers import *

engine = db.create_engine(r"sqlite:///route66_db.sqlite3")
session_creater_=sessionmaker(bind=engine)
def session_creater():
    return scoped_session(session_creater_)
connection = engine.connect()
metadata = db.MetaData()

UPLOAD_FOLDER = './static'
ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'])
app = Flask(__name__,static_url_path='/static')
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
CORS(app)

def create_connection(db_file):
    """ create a database connection to a SQLite database """
    conn = None
    try:
        conn = sqlite3.connect(db_file)
    except Error as e:
        print(e)
    return conn


def init_db():
    conn = create_connection(r"./route66_db.sqlite3")
    cursor=conn.cursor()
    cursor.execute('DROP TABLE IF EXISTS USER')
    cursor.execute('DROP TABLE IF EXISTS Task')
    cursor.execute('DROP TABLE IF EXISTS Flight')
    cursor.execute('DROP TABLE IF EXISTS LogFile')
    cursor.execute('DROP TABLE IF EXISTS TaskInfo')
    cursor.execute('DROP TABLE IF EXISTS Video')
    cursor.execute('DROP TABLE IF EXISTS TaskDependency')
    #cursor.execute("insert into Task values(1,'Flight Esenboğa to Metu CENG','C:\\Users\\Egemen\\Desktop\\2020_FALL\\CENG491\\LogFiles\\METU')")
    #cursor.execute("insert into Task values(2,'Flight Esenboğa to Metu EE','C:\\Users\\Egemen\\Desktop\\2020_FALL\\CENG491\\LogFiles\\METU')")
    print("Table created successfully");
    conn.commit()

    conn.close() 
 
 



@app.route('/api/v1/welcome')
def serve_welcome():
    return send_file("./static/welcome.html")

@app.route("/health-check")
def health_check():
    return jsonify(success=True)


if __name__ == "__main__":
    
    # init_db()
    session=session_creater()
    Base.metadata.create_all(engine, checkfirst=True)

    # user1=User('furkan','123456','furkan','akyol')
    # session.add(user1)

    # logfile1=LogFile(log_file_name='first log',log_file_path='./log-file/OnePass2.txt',user_id=1)
    # session.add(logfile1)

    # taskinfo1=TaskInfo(width= 1920, height= 1080, fov= 90, framerate= 30, camera_pan= 0, camera_tilt= -20, camera_roll= 0, samplerate= 100, 
    # time_offset= 0, #datetime.datetime.strptime("Tue, 22 Nov 2011 06:00:00 GMT", "%a, %d %b %Y %H:%M:%S %Z"), 
    # renderer_params= { 'renderer':"cesiumjs", 'skybox':1, 'lighting_mode':1, 'light_fade_dist':0, 'night_fade_dist':0, 'shadows':1, 'terrain_shadow_mode':3, 'show_ground_atm_mode':0, 'terrain_depth_test':1, 'time_advance_mode':0, 'water_mask':1, 'vertex_normals':1, 'frustum_near': 1.0, 'frustum_far':1e+8})
    # session.add(taskinfo1)
    session.commit()
    session.close()
    print('committed')
    
    from views import *
    from render_server_views import *
    app.run(port=5000)


def test():
    init_db()
    Base.metadata.create_all(engine, checkfirst=True)

    user1=User('furkan','123456','furkan','akyol')
    session.add(user1)


    logfile1=LogFile(log_file_name='first log',log_file_path='./log-file/OnePass.txt',user_id=1)
    session.add(logfile1)

    taskinfo1=TaskInfo(width= 1920, height= 1080, fov= 90, framerate= 30, camera_pan= 0, camera_tilt= -20, camera_roll= 0, samplerate= 100, 
    time_offset= 0, #datetime.datetime.strptime("Tue, 22 Nov 2011 06:00:00 GMT", "%a, %d %b %Y %H:%M:%S %Z"), 
    renderer_params= { 'renderer':"cesiumjs", 'skybox':1, 'lighting_mode':1, 'light_fade_dist':0, 'night_fade_dist':0, 'shadows':1, 'terrain_shadow_mode':3, 'show_ground_atm_mode':0, 'terrain_depth_test':1, 'time_advance_mode':0, 'water_mask':1, 'vertex_normals':1, 'frustum_near': 1.0, 'frustum_far':1e+8})
    session.add(taskinfo1)

    task1=Task('naber',session.query(LogFile).first().log_file_id,0,session.query(User).first().user_id, session.query(TaskInfo).first().task_info_id)
    session.add(task1)

    session.commit()
    result=session.query(User).first().owned_task[0].task_info
    print(result)
    return result
