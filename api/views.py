from main import app, session_creater
from functools import wraps
from flask import jsonify,request,redirect,url_for,Response,send_from_directory
from models_ import *
from serializers import *
import os
import re
from helpers import _make_success,_make_error,get_chunk,session_scope
from werkzeug.utils import secure_filename
import requests

logged_out_tokens=set()
def login_required(f):
    @wraps(f)
    def decorated_function(*args, **kws):
        if "application/json" in request.content_type:
            request_data=request.get_json(force=True)
        elif "form-data" in request.content_type:
            request_data=request.form.to_dict()
        else:
            (_make_error(400,'Unknown mime type. You should only use application/json or form-data'))

        if not 'token' in request_data.keys():
            return (_make_error(401,'no token found'))
        token = request_data['token']
        if token in logged_out_tokens:
        	return (_make_error(401,'you logged out please login again'))
        user_id = decode_auth_token(token)
        with session_scope() as session:
            try:
                res=session.query(User).filter(User.user_id == user_id).first()
                if res is not None:
                    return f(request_data,user_id)
                else:
                    return (_make_error(404,'token not valid'))
            except Exception as e:
                print(e)
    return decorated_function



@app.route("/api/v1/sign-up",methods=['post'])
def sign_in():
    requestData=request.get_json()
    username=requestData['user_name']
    password=requestData['password']
    name=requestData['name']
    surname=requestData['surname']
    try:
        session=session_creater()
        res=session.query(User).filter(User.username == username).all()
        if len(res)>0:
            return (_make_error(801,'user name already taken'))
        else:
            new_user=User(username,str(password),name,surname)
            session.add(new_user)
            session.commit()
            return (_make_success({},'user created'))
    except Exception as e:
        print(e)
        (_make_error({},'user created'))
        session.rollback()
    session.close()


"""
{
    "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1NzcyMTA2NDIsImlhdCI6MTU3NzIwNzA0Miwic3ViIjoxfQ.9_Q6_JXcsTNX8--SZZJJ9nY28etA-8VbwHfpXmeUh48"
}
"""
@app.route("/api/v1/login",methods=['post'])
def login():
    requestData=request.get_json()
    username=requestData['user_name']
    password=requestData['password']
    with session_scope() as session:
        try:
            res=session.query(User).filter(User.username == username).first()
            if res is not None and res.check_password(password):
                token=encode_auth_token(res.user_id)
                return jsonify({'token': token.decode('utf-8')})
            return (_make_error(404,'no such user'))
        except Exception as e:
            print(e)
            return (_make_error(404,str(e)))

"""
{
    "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1NzcyMTA2NDIsImlhdCI6MTU3NzIwNzA0Miwic3ViIjoxfQ.9_Q6_JXcsTNX8--SZZJJ9nY28etA-8VbwHfpXmeUh48"
}
"""
@app.route("/api/v1/logout",methods=['post'])
@login_required
def logout(req,user_id):
	logged_out_tokens.add(req['token'])
	return jsonify({'message':'success'})


@app.route('/api/v1/all-tasks', methods=['get','post'])
def all_tasks():
    try:
        session=session_creater()
        result=session.query(Task).all()
        session.close()
        return jsonify([task_serializer_for_render_server(x) for x in result ])
    except Exception as e:
        print(e)


@app.route('/api/v1/progress', methods=['post'])
def progress():
    try:
        r=requests.post('http://127.0.0.1:5001/progress', {task_id:request.json()['task_id']})
        resp=r.json()
        if (r.status==200):
            return jsonify(resp)
    except Exception as e:
        print(e)


"""
    it is get request no token validation for video serving.
    It can serve as packages or with html video tag and src.

    it can serve all videos in static/video folder based on the video-name parameter given in the url
"""
@app.route('/video')
def serve_video():
    video_name=request.args.get('name')
    range_header = request.headers.get('Range', None)
    byte1, byte2 = 0, None
    if range_header:
        match = re.search(r'(\d+)-(\d*)', range_header)
        groups = match.groups()

        if groups[0]:
            byte1 = int(groups[0])
        if groups[1]:
            byte2 = int(groups[1])

    file_path=os.path.join(app.config['UPLOAD_FOLDER'], 'video', video_name).replace("\\","/")
    chunk, start, length, file_size = get_chunk(file_path,byte1, byte2)
    resp = Response(chunk, 206, mimetype='video/mp4',
                      content_type='video/mp4', direct_passthrough=True)
    resp.headers.add('Content-Range', 'bytes {0}-{1}/{2}'.format(start, start + length - 1, file_size))
    return resp


@app.route('/log-file')
def serve_log_file():
    file_name=request.args.get('name')
    return send_from_directory(os.path.join(app.config['UPLOAD_FOLDER'] ,'log_file'), file_name)


"""
Upload a video file by only sending metadata about it such as path and name 
to save necessary records in the database. 

This endpoint should only be used if video file is saved in the static file of the
backend server for sure. Otherwise database would point to an unexisting path. 
"""
@app.route('/api/v1/upload-video-metadata',methods=['post'])
def upload_file_without_file():
    requestData=request.get_json()

    video_name = secure_filename(requestData['video_name'])
    related_task_id=requestData['task_id']

    #TODO: wont work for windows 
    path=os.path.join(app.config['UPLOAD_FOLDER'], '.mp4' ,video_name).replace("\\","/")

    #TODO: file name and owner is ignored for now they should be filled accordingly based on the info in the request 
    new_item=Video('video',path,related_task_id)
    try:
        session=session_creater()
        session.add(new_item)
        session.commit()
    except Exception as e:
        print(e)
        return _make_error(805,'{} could not be saved to database'.format(request.form['file_type']))
    return _make_success({},'file uploaded succesfully')


"""
user endpoints:
"""
#to create a new task.
#should be login required.
@app.route('/api/v1/create-task',methods=['post'])
@login_required
def create_task(req,user_id):
    with session_scope() as session:
        try:
            data=request.get_json()
            task_info=TaskInfo(**data['task_info'])
            session.add(task_info)
            session.flush()


            #get log file id, if it is a fog task log file id automatically
            #generated by assigning original task's log file id
            if data['task_info']['renderer_params']['renderer']=="fogcloud":
                original_task=session.query(Task).filter(Task.task_id==data['task_info']['renderer_params']["original_task_id"]).first()
                if original_task is not None:
                    log_file_id=original_task.log_file.log_file_id
                else:
                    #TODO: in production remove below line and uncomment the one under it
                    log_file_id=1
                   #return _make_error(400,'original task id not recognized')
            else: 
                log_file_id=data['log_file_id']
            
            
            task=Task(data['task_name'],
                log_file_id,
                0,
                user_id, 
                task_info.task_info_id) # task info will also be get from user
            session.add(task)
            session.flush()
            for dependency in data['dependencies']:
                d=TaskDependency(depending=task.task_id, dependent=dependency)
                session.add(d)
            session.flush()
            return jsonify({'message':'success','created':task_serializer_for_user(task)})
        except Exception as e:
            print(e)
            return _make_error(400,str(e))

# upload log-file
@app.route('/api/v1/upload-log-file', methods=['post'])
@login_required
def upload_log_file(req, user_id):
    if 'file' not in request.files:
        print('No file part')
        return _make_error(400, 'no file part')
    file = request.files['file']

    if file.filename == '':
        print('No selected file')
        return _make_error(400, 'No selected file')

    if file:  # and allowed_file(file.filename):
        filename = secure_filename(file.filename)
        path = os.path.join(app.config['UPLOAD_FOLDER'], "log_file", filename).replace("\\", "/")
        print(path)
        session = session_creater()
        l = LogFile(log_file_name=request.form['log_file_name'], log_file_path=path, user_id=user_id)
        session.add(l)
        session.commit()
        session.close()
        file.save(path)
        return jsonify({'message': 'success'})
    else:
        return _make_error(400, 'file is none')


#endpint to access log files of a user
@app.route('/api/v1/my-files',methods=['post'])
@login_required
def my_files(req,user_id):
    try:
        session=session_creater()
        result=session.query(LogFile).filter(LogFile.user_id==user_id).all()
        session.close()
        if result is None:
            return jsonify({'message':'success', 'files':[]})
        else:
            return jsonify({'message':'success', 'files':[log_file_serializer(x) for x in result] })
    except Exception as e:
        print(e)


#endpoint to access tasks of a user
#progress info of tasks can be seen here 
@app.route('/api/v1/my-tasks',methods=['post'])
@login_required
def my_tasks(req,user_id):
    with session_scope() as session:
        try:
            result=session.query(Task).filter(Task.user==user_id).all()
            if result is None:
                return jsonify({'message':'success', 'files':[]})
            else:
                return jsonify({'message':'success', 'tasks':[task_serializer_for_user(x) for x in result] })
        except Exception as e:
            print(e)


#endpint to access log files of a user
@app.route('/api/v1/my-videos',methods=['post'])
@login_required
def my_videos(req,user_id):
    try:
        session=session_creater()
        tasks=session.query(Task).filter(Task.user==user_id).all()
        videos=[task.video[0] for task in tasks]
        print(videos)
        if videos is None:
            return jsonify({'message':'success', 'files':[]})
        else:
            return jsonify({'message':'success', 'videos':[video_serializer(video) for video in videos] })
    except Exception as e:
        print(e)
