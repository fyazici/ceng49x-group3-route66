import os
from flask import jsonify
from contextlib import contextmanager
from main import session_creater
from models_ import Task


@contextmanager
def session_scope():
    """Provide a transactional scope around a series of operations."""
    session = session_creater()
    try:
        yield session
        session.commit()
    except:
        session.rollback()
        raise
    finally:
        session.close()

logged_out_tokens=set()
def _make_error(status_code, message):
    response = jsonify({
        'status': status_code,
        'message': message
    })
    return response


def _make_success(data,message):
    response = jsonify({
        'message': message,
        'data': data
    })
    return response


def get_chunk(file_path,byte1=None, byte2=None):
    full_path = file_path
    file_size = os.stat(full_path).st_size
    start = 0
    length = 102400

    if byte1 < file_size:
        start = byte1
    if byte2:
        length = byte2 + 1 - byte1
    else:
        length = file_size - start

    with open(full_path, 'rb') as f:
        f.seek(start)
        chunk = f.read(length)
    return chunk, start, length, file_size


"""
input:task -> it is a sqlalchemy Task object and it is given as an input
input: session -> sqlalchemy session object


returns true if task's all dependenies are finished
returns false if all dependencies are not met 
"""
def is_ready(task,session):
    for dep in task.dependencies:
        status=session.query(Task).filter(Task.task_id==dep.dependent).first().status
        if status==0 or status==2:
            return False
    return True
