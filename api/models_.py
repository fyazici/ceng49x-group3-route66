from sqlalchemy import Table, Column, Integer, String, MetaData, ForeignKey, Boolean, DateTime, Float, JSON
from sqlalchemy.orm import relationship
from base import Base
import jwt
import datetime
from sqlalchemy import event
from werkzeug.security import generate_password_hash, check_password_hash
def encode_auth_token(user_id):
    """
    Generates the Auth Token
    :return: string
    """
    try:
        payload = {
            'exp': datetime.datetime.utcnow() + datetime.timedelta(days=0, minutes=60),
            'iat': datetime.datetime.utcnow(),
            'sub': user_id
        }
        return jwt.encode(
            payload,
            'fikibok', #secret key
            algorithm='HS256'
        )
    except Exception as e:
        return e
def decode_auth_token(auth_token):
    """
    Decodes the auth token
    :param auth_token:
    :return: integer|string
    """
    try:
        payload = jwt.decode(auth_token, 'fikibok')
        return payload['sub']
    except jwt.ExpiredSignatureError:
        return 'Signature expired. Please log in again.'
    except jwt.InvalidTokenError:
        return 'Invalid token. Please log in again.'


class User(Base):
    __tablename__ = "User"
    user_id=Column(Integer, primary_key=True)
    username=Column(String)
    password=Column(String)
    name=Column(String)
    surname=Column(String)
    
    owned_task = relationship("Task")
    owned_logs=relationship("LogFile")
    

    def __init__(self,username,password,name,surname):
        self.username=username
        self.name=name
        self.surname=surname
        self.set_password(password)

    
    def set_password(self, password):
        self.password = generate_password_hash(password)

    
    def check_password(self, password):
        return check_password_hash(self.password, password)
    

class TaskDependency(Base):
    __tablename__ = "TaskDependency"
    task_dependency_id=Column(Integer, primary_key=True)
    
    #depending task depends on dependent task
    depending=Column(Integer,ForeignKey('Task.task_id'))
    dependent=Column(Integer,ForeignKey('Task.task_id'))

    
    def check_password(self, password):
        return check_password_hash(self.password, password)
    

class Task(Base):
    __tablename__ = "Task"
    task_id=Column(Integer, primary_key=True)
    task_name=Column(String)
    status=Column(Integer)
    progress=Column(Integer, default=0)

    log_file_id=Column(Integer, ForeignKey('LogFile.log_file_id'))
    log_file = relationship('LogFile', back_populates='tasks')

    video=relationship("Video")

    #one to one relationship
    task_info = relationship("TaskInfo", uselist=False, back_populates="task")
    task_info_id = Column(Integer, ForeignKey('TaskInfo.task_info_id'))
    
    #preconditions
    #this is a list of TaskDependency objects of which dependent field is the task that this task depends on
    dependencies=relationship("TaskDependency", foreign_keys='TaskDependency.depending')

    user = Column(Integer, ForeignKey('User.user_id'))    
    

    def __init__(self,taskname,log_file_id,status,user,task_info_id):
        self.task_name=taskname
        self.status=status
        self.log_file_id=log_file_id
        self.user=user
        self.task_info_id=task_info_id
    
"""
After new task assigned this event is triggered to inform renderer if it is sleeping
"""    
@event.listens_for(Task, 'after_insert')
def receive_after_insert(mapper, connection, target):
    #TODO: move events to another file and import this there
    import requests
    requests.post('http://127.0.0.1:5001/wake-up')




class TaskInfo(Base):
    __tablename__= "TaskInfo"
    task_info_id = Column(Integer, primary_key=True)
    width = Column(Integer)
    height = Column(Integer)
    fov = Column(Float)
    framerate = Column(Integer)
    camera_pan = Column(Float)
    camera_tilt = Column(Float)
    camera_roll = Column(Float)
    renderer_params= Column(JSON)

    task = relationship("Task", uselist=False, back_populates="task_info")


class Video(Base):
    __tablename__ = "Video"
    video_id=Column(Integer, primary_key=True)
    video_name=Column(String)
    video_file_path=Column(String)
    is_finished=Column(Boolean)
    parent_id = Column(Integer, ForeignKey('Task.task_id'),nullable=True)
    
    def __init__(self,video_name,video_file_path,parent_id):
        self.video_name=video_name
        self.video_file_path=video_file_path
        self.parent_id=parent_id


class LogFile(Base):
    __tablename__ = "LogFile"
    log_file_id  = Column(Integer, primary_key=True)
    log_file_name  = Column(String)
    log_file_path = Column(String)

    user_id=Column(Integer, ForeignKey('User.user_id'),nullable=True)


    tasks=relationship('Task',back_populates="log_file")
