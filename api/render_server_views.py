 
from main import app, session_creater
from functools import wraps
from flask import jsonify,request,redirect,url_for,Response,send_from_directory
from models_ import *
from serializers import *
import os
import re
from helpers import _make_success,_make_error,get_chunk, is_ready,session_scope
from werkzeug.utils import secure_filename
from sqlalchemy.orm import joinedload
import requests

@app.route('/api/v1/task-finished', methods=['post'])
def task_finished():
    try:
        with session_scope() as session:
            req=request.get_json()
            result=session.query(Task).filter(Task.task_id==req['task_id']).first()
            if result is None:
                #no such task
                pass
            else:
                result.status=1
                result.progress=100
                session.commit()
                return jsonify({'message':'success'})
    except Exception as e:
        session.rollback()
        session.close()
        return (_make_error(404,str(e)))


@app.route('/api/v1/new-task', methods=['get','post'])
def new_task():
        try:
            with session_scope() as session:
                result=session.query(Task).options(joinedload('dependencies')).filter(Task.status==0).all()
                if (len(result)>0):

                    #check tasks' dependencies
                    for i,res in enumerate(result):
                        if is_ready(res,session):
                            #a task is ready break loop and send it
                            result=res
                            break
                        if i==len(result)-1:
                            # all tasks are depending on unfinished tasks
                            return jsonify({'message':'sleep'})

                    result.status=2
                    session.commit()
                    return jsonify({'message':'success',
                                    'task':task_serializer_for_render_server(result)})
                else:
                    #no waiting tasks send sleep message
                    return jsonify({'message':'sleep'})
        except Exception as e:
            print(e)
            return None


"""
Upload a video file by sending the metadata and the video file itself.

This endpoint is to be used if render server and backend server will run on
different computers and communicate via internet.

If they will be on the same computer it is unnecessary to send video via 
internet. Instead use below endpoint
"""
@app.route('/api/v1/upload',methods=['post'])
def upload_file():
    if request.method == 'POST':
        if 'file' not in request.files:
            print('No file part')
            return _make_error(802,'no file part')
        file = request.files['file']

        if file.filename == '':
            print('No selected file')
            return _make_error(804,'No selected file')

        if file: # and allowed_file(file.filename):
            if 'file_type' not in request.form:
                print('no file type specified')
                return _make_error(802,'no file type specified')

            filename = secure_filename(file.filename)
            path=os.path.join(app.config['UPLOAD_FOLDER'], request.form['file_type'] ,filename).replace("\\","/")
            file.save(path)
            

            #TODO: file name and owner is ignored for now they should be filled accordingly based on the info in the request 
            new_item=Video('video',path,None) if request.form['file_type']=='video' else LogFile(log_file_name='logfile',log_file_path=path)
            try:
                session=session_creater()
                session.add(new_item)
                session.commit()
                session.close()
            except Exception as e:
                session.close()
                print(e)
                return _make_error(805,'{} could not be saved to database'.format(request.form['file_type']))
            return _make_success({},'file uploaded succesfully')


# TODO: we cannot extend behaviour of this endpoint
@app.route("/upload/<filename>", methods=["POST"])
def upload(filename):
    with open(os.path.join("uploads", filename), "wb") as f:
        for chunk in request.stream:
            f.write(chunk)
    return _make_success({},'file uploaded succesfully')



"""
monitoring views 
"""
@app.route('/api/v1/set-progress', methods=['post'])
def set_progress():
    try:
        req=request.get_json()
        session=session_creater()
        result=session.query(Task).filter(Task.task_id==req['task_id']).first()
        if result is None:
            #no such task
            pass
        else:
            result.progress=req['progress']
            session.commit()
            session.close()
            return jsonify({'message':'success'})
    except Exception as e:
        session.close()
        print(e)