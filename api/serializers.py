def task_serializer_for_render_server(task):
    return {    
                "task_id":task.task_id,
                "user_id":task.user,
                "name":task.task_name,
                "width": task.task_info.width,
                "height": task.task_info.height,
                "fov": task.task_info.fov,
                "framerate": task.task_info.framerate,
                "camera_pan": task.task_info.camera_pan,
                "camera_tilt": task.task_info.camera_tilt,
                "camera_roll": task.task_info.camera_roll,
                "missionlog_path": task.log_file.log_file_path,
                "renderer_params": task.task_info.renderer_params,
                "output_path": "../api/static/video/task{}.mp4".format(task.task_id), # TODO: change it 
                # pix_fmt and faststart is important for streamable video output
                "encoding_params": [ # TODO: change it 
                    "-profile:v", "high",
                    "-pix_fmt", "yuv420p",
                    "-movflags", "faststart"
                ]
            }
def task_serializer_for_user(task):
    return {    
                "status": "rendering" if task.status==2 else ("queued" if task.status==0 else "finished"),
                "progress":task.progress,
                "task_id":task.task_id,
                "name":task.task_name,
                "width": task.task_info.width,
                "height": task.task_info.height,
                "fov": task.task_info.fov,
                "framerate": task.task_info.framerate,
                "camera_pan": task.task_info.camera_pan,
                "camera_tilt": task.task_info.camera_tilt,
                "camera_roll": task.task_info.camera_roll,
                "missionlog_path": task.log_file.log_file_path,
                "log_file_id": task.log_file_id,
                "renderer_params": task.task_info.renderer_params
            }

def log_file_serializer(log_file):
    return {    
                "log_file_id":log_file.log_file_id,
                "log_file_name": log_file.log_file_name,
                "log_file_path": log_file.log_file_path
            }

def video_serializer(video):
    return {    
                "video_name":video.video_name,
                "video_file_path": video.video_file_path,
            }
