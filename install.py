# Simple installer for one-time configuration
import subprocess
import pathlib
import json
import sys
import os
import argparse


def install_backend(config):
    print("\nInstall backend dependencies")
    subprocess.call(["pip", "install", "-r", "api/requirements.txt"])

def install_renderer(config):
    print("\nConfigure Render Server")

    # install dependencies
    print("\nInstall renderer dependencies")
    subprocess.call(["pip", "install", "-r", "renderer/requirements.txt"])
    # pycuda needs cython but the package info is broken. install separately
    subprocess.call(["pip", "install", "pycuda==2019.1.2"])

    # set max concurrent renders
    n_max_renders = input("\nEnter the maximum number of concurrent renderers:\n(default=2) ")
    if n_max_renders == "":
        n_max_renders = 2
    else:
        n_max_renders = int(n_max_renders)
    config["R66_MAX_CONCURRENT_RENDERS"] = n_max_renders

    # create headless profiles
    print("\n>>> Create Firefox user profiles for Cesium workers...", end="", flush=True)
    subprocess.call([sys.executable, "create_profiles.py", str(5002), str(5002 + n_max_renders)], cwd="renderer/cesium/headless_profiles/")
    print(" OK")

    # set firefox executable
    if sys.platform.startswith("win"):
        firefox_exec_default = r"C:\Program Files\Mozilla Firefox\firefox.exe"
    else:
        firefox_exec_default = r"firefox"
    firefox_exec = input("\nEnter the path for Firefox executable:\n(default={}) ".format(firefox_exec_default))
    if firefox_exec == "":
        firefox_exec = firefox_exec_default
    print("\n>>> Test Firefox executable... ", end="", flush=True)
    try:
        r = subprocess.call([firefox_exec, "-v"], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
        if r == 0:
            print(" OK")
        else:
            print("FAILED (return code {})".format(r))
    except Exception as e:
        print("FAILED (thrown {})".format(e))
    config["R66_FIREFOX_EXECUTABLE"] = firefox_exec

    # set ffmpeg executable
    if sys.platform.startswith("win"):
        ffmpeg_exec_default = str(pathlib.Path("redist/ffmpeg/bin/ffmpeg.exe").resolve())
    else:
        ffmpeg_exec_default = "ffmpeg"
    ffmpeg_exec = input("\nEnter the path for ffmpeg executable with h264_nvenc support:\n(default={}) ".format(ffmpeg_exec_default))
    if ffmpeg_exec == "":
        ffmpeg_exec = ffmpeg_exec_default
    print("\n>>> Test ffmpeg executable... ", end="", flush=True)
    try:
        ffmpeg_proc = subprocess.Popen([ffmpeg_exec, "-codecs"], text=True, stdout=subprocess.PIPE, stderr=subprocess.DEVNULL)
        ffmpeg_stdout, _ = ffmpeg_proc.communicate(None)
        ffmpeg_proc.wait()
        if ffmpeg_proc.returncode == 0:
            if "h264_nvenc" in ffmpeg_stdout:
                print(" OK")
            else:
                print("FAILED (h264_nvenc codec not supported)")
        else:
            print("FAILED (return code {})".format(r))
    except Exception as e:
        print("FAILED (thrown {})".format(e))
    config["R66_FFMPEG_EXECUTABLE"] = ffmpeg_exec

    # check nvcc
    nvcc_append_dir = input("\nAppend this to PATH for nvcc executable:\n(default:nothing)")
    print("\n>>> Test nvcc executable... ", end="", flush=True)
    try:
        env = os.environ.copy()
        if nvcc_append_dir != "":
            env["PATH"] += os.pathsep + nvcc_append_dir
        r = subprocess.call(["nvcc", "--version"], env=env, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
        if r == 0:
            print(" OK")
        else:
            print("FAILED (return code {})".format(r))
    except Exception as e:
        print("FAILED (thrown {})".format(e))
    config["R66_NVCC_APPEND_DIR"] = nvcc_append_dir

    # check c++ compiler
    if sys.platform.startswith("win"):
        cpp_cmd = ["cl.exe"]
        try:
            p = pathlib.Path(r"C:\Program Files (x86)\Microsoft Visual Studio\2019\BuildTools\VC\Tools\MSVC")
            cpp_append_dir_default = str(list(p.glob("*"))[0].joinpath(r"bin\Hostx64\x64"))
        except Exception as e:
            print(e)
            cpp_append_dir_default = "nothing"
    else:
        cpp_cmd = ["g++", "--version"]
        cpp_append_dir_default = "nothing"
    cpp_append_dir = input("\nAppend this to PATH for C++ compiler ({}):\n(default:{})".format(cpp_cmd[0], cpp_append_dir_default))
    if cpp_append_dir == "" and cpp_append_dir_default != "nothing":
        cpp_append_dir = cpp_append_dir_default
    print("\n>>> Test C++ compiler...", end="", flush=True)
    try:
        env = os.environ.copy()
        if cpp_append_dir != "":
            env["PATH"] += os.pathsep + cpp_append_dir
        r = subprocess.call(cpp_cmd, env=env, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
        if r == 0:
            print(" OK")
        else:
            print("FAILED (return code {})".format(r))
    except Exception as e:
        print("FAILED (thrown {})".format(e))
    config["R66_CPP_APPEND_DIR"] = cpp_append_dir

    # cython module
    print("\n>>> Compile Cython module for fast_dem_provider...", end="", flush=True)
    # cython needs c++ compiler so prepare path
    env = os.environ.copy()
    if cpp_append_dir != "":
        env["PATH"] += os.pathsep + cpp_append_dir
    dem_util_dir = "renderer/depth/util/"
    r = subprocess.call([sys.executable, "setup.py", "build_ext", "--inplace"], cwd=dem_util_dir, env=env, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
    if r == 0:
        print(" OK")
    else:
        print("FAILED (return code {})".format(r))
    
    # fog perlin noise module
    print("\n>>> Compile shared libraries for fog/cloud effects...", end="", flush=True)
    # perlin needs c++ compiler so prepare path
    env = os.environ.copy()
    if cpp_append_dir != "":
        env["PATH"] += os.pathsep + cpp_append_dir
    env = os.environ.copy()
    if cpp_append_dir != "":
        env["PATH"] += os.pathsep + cpp_append_dir
    effects_modules_dir = "renderer/effects/modules/"
    r = subprocess.call([sys.executable, "setup.py"], cwd=effects_modules_dir, env=env, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
    if r == 0:
        print(" OK")
    else:
        print("FAILED (return code {})".format(r))

def install_frontend(config):
    print("\nInstall frontend dependencies")
    subprocess.call(["pip", "install", "-r", "ui/requirements.txt"])

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--init-venv", action="store_true", help="Initialize a suitable virtual environment and exit")
    args = parser.parse_args()

    if args.init_venv:
        subprocess.call([sys.executable, "-m", "venv", "env"])
        print("Virtual environment 'env' is created")
        print("Please activate this environment using:")
        print("  on Windows:    env\\Scripts\\activate")
        print("  on Linux:      . env/bin/activate")
        print("and then run this installer --disable-venv parameter.")
        exit(0)

    config = {}
    install_backend(config)
    install_renderer(config)
    install_frontend(config)

    print("\nGenerated configuration: (config.json)")
    print(json.dumps(config, indent=4))
    with open("config.json", "w") as f:
        json.dump(config, f, indent=4)
