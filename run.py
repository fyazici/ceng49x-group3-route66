# Simple runner for Route66 components

import subprocess
import os
import json
import sys
import webbrowser
import requests
import time

if __name__ == "__main__":
    # load Route66 config
    with open("config.json") as f:
        config = json.load(f)
    
    # setup environment variables
    env = os.environ.copy()
    env["R66_MAX_CONCURRENT_RENDERS"] = str(config["R66_MAX_CONCURRENT_RENDERS"])
    env["R66_FIREFOX_EXECUTABLE"] = str(config["R66_FIREFOX_EXECUTABLE"])
    env["R66_FFMPEG_EXECUTABLE"] = str(config["R66_FFMPEG_EXECUTABLE"])

    # append to path
    env["PATH"] += os.pathsep + str(config["R66_NVCC_APPEND_DIR"])
    env["PATH"] += os.pathsep + str(config["R66_CPP_APPEND_DIR"])
    
    print(">>> Starting up Route66 <<<\n\n")

    # start render server
    renderer_proc = subprocess.Popen([sys.executable, "render-server.py"], cwd="renderer/", env=env)
    for i in range(10):
        try:
            requests.get("http://127.0.0.1:5001/health-check")
            break
        except Exception as e:
            print("Waiting render server: {}".format(i))
            time.sleep(1)
    else:
        print("Could not start render server")
        exit(1)
    # start backend
    backend_proc = subprocess.Popen([sys.executable, "main.py"], cwd="api/", env=env)
    for i in range(10):
        try:
            requests.get("http://127.0.0.1:5000/health-check")
            break
        except Exception as e:
            print("Waiting backend: {}".format(i))
            time.sleep(1)
    else:
        print("Could not start backend")
        exit(1)
    # start frontend
    frontend_proc = subprocess.Popen([sys.executable, "react-server.py"], cwd="ui/", env=env)
    for i in range(10):
        try:
            requests.get("http://127.0.0.1:5555/health-check")
            break
        except Exception as e:
            print("Waiting frontend: {}".format(i))
            time.sleep(1)
    else:
        print("Could not start frontend")
        exit(1)
    webbrowser.open("http://127.0.0.1:5555/")

    try:
        backend_proc.wait()
        renderer_proc.wait()
        frontend_proc.wait()
    except KeyboardInterrupt as _:
        print("\n\n>>> Shutting down Route66 <<<")
        backend_proc.kill()
        renderer_proc.kill()
        frontend_proc.kill()
        backend_proc.wait()
        renderer_proc.wait()
        frontend_proc.wait()
    